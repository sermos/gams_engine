# gams_engine.AuthApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_jwt_token**](AuthApi.md#create_jwt_token) | **POST** /auth/ | Creates a JSON Web Token(JWT) for authentication (username and password in request header via Basic auth)
[**create_jwt_token_json**](AuthApi.md#create_jwt_token_json) | **POST** /auth/login | Creates a JSON Web Token(JWT) for authentication (username and password in request body)
[**invalidate_jwt_tokens**](AuthApi.md#invalidate_jwt_tokens) | **POST** /auth/logout | Invalidates all your JWT


# **create_jwt_token**
> ModelAuthToken create_jwt_token()

Creates a JSON Web Token(JWT) for authentication (username and password in request header via Basic auth)

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import auth_api
from gams_engine.model.model_auth_token import ModelAuthToken
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)
    expires_in = 14400 # int | Time (in seconds) when the token expires. (optional) if omitted the server will use the default value of 14400

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Creates a JSON Web Token(JWT) for authentication (username and password in request header via Basic auth)
        api_response = api_instance.create_jwt_token(expires_in=expires_in)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling AuthApi->create_jwt_token: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expires_in** | **int**| Time (in seconds) when the token expires. | [optional] if omitted the server will use the default value of 14400

### Return type

[**ModelAuthToken**](ModelAuthToken.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Request |  -  |
**401** | Unauthorized |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_jwt_token_json**
> ModelAuthToken create_jwt_token_json(username, password)

Creates a JSON Web Token(JWT) for authentication (username and password in request body)

### Example


```python
import time
import gams_engine
from gams_engine.api import auth_api
from gams_engine.model.model_auth_token import ModelAuthToken
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with gams_engine.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)
    username = "username_example" # str | 
    password = "password_example" # str | 
    expires_in = 14400 # int | Time (in seconds) when the token expires. (optional) if omitted the server will use the default value of 14400
    access_scopes = [
        "READONLY",
    ] # [str] | Access scopes. Possible values: READONLY NAMESPACES JOBS USERS HYPERCUBE CLEANUP LICENSES USAGE (optional)

    # example passing only required values which don't have defaults set
    try:
        # Creates a JSON Web Token(JWT) for authentication (username and password in request body)
        api_response = api_instance.create_jwt_token_json(username, password)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling AuthApi->create_jwt_token_json: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Creates a JSON Web Token(JWT) for authentication (username and password in request body)
        api_response = api_instance.create_jwt_token_json(username, password, expires_in=expires_in, access_scopes=access_scopes)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling AuthApi->create_jwt_token_json: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**|  |
 **password** | **str**|  |
 **expires_in** | **int**| Time (in seconds) when the token expires. | [optional] if omitted the server will use the default value of 14400
 **access_scopes** | **[str]**| Access scopes. Possible values: READONLY NAMESPACES JOBS USERS HYPERCUBE CLEANUP LICENSES USAGE | [optional]

### Return type

[**ModelAuthToken**](ModelAuthToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Request |  -  |
**401** | Unauthorized |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **invalidate_jwt_tokens**
> Message invalidate_jwt_tokens()

Invalidates all your JWT

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import auth_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Invalidates all your JWT
        api_response = api_instance.invalidate_jwt_tokens()
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling AuthApi->invalidate_jwt_tokens: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Request |  -  |
**401** | Unauthorized |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

