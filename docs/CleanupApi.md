# gams_engine.CleanupApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**list_results**](CleanupApi.md#list_results) | **GET** /cleanup/results | Lists all the job results and Hypercube results
[**remove_results**](CleanupApi.md#remove_results) | **DELETE** /cleanup/results | Deletes job results and Hypercube results


# **list_results**
> CleanableJobResultPage list_results()

Lists all the job results and Hypercube results

If the page is not one and there are no elements on this page, throws 404. Requires admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import cleanup_api
from gams_engine.model.message import Message
from gams_engine.model.cleanable_job_result_page import CleanableJobResultPage
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cleanup_api.CleanupApi(api_client)
    page = 1 # int |  (optional) if omitted the server will use the default value of 1
    per_page = 0 # int |  (optional) if omitted the server will use the default value of 0
    order_by = "upload_date" # str |  (optional) if omitted the server will use the default value of "upload_date"
    order_asc = False # bool |  (optional) if omitted the server will use the default value of False
    from_datetime = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | iso8601 Datetime to select the results if they are uploaded before. (optional)
    to_datetime = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | iso8601 Datetime to select the results if they are uploaded after. (optional)
    namespace = "namespace_example" # str | Filter results by namespace. (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Lists all the job results and Hypercube results
        api_response = api_instance.list_results(page=page, per_page=per_page, order_by=order_by, order_asc=order_asc, from_datetime=from_datetime, to_datetime=to_datetime, namespace=namespace)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling CleanupApi->list_results: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional] if omitted the server will use the default value of 1
 **per_page** | **int**|  | [optional] if omitted the server will use the default value of 0
 **order_by** | **str**|  | [optional] if omitted the server will use the default value of "upload_date"
 **order_asc** | **bool**|  | [optional] if omitted the server will use the default value of False
 **from_datetime** | **datetime**| iso8601 Datetime to select the results if they are uploaded before. | [optional]
 **to_datetime** | **datetime**| iso8601 Datetime to select the results if they are uploaded after. | [optional]
 **namespace** | **str**| Filter results by namespace. | [optional]

### Return type

[**CleanableJobResultPage**](CleanableJobResultPage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | Page not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_results**
> Message remove_results()

Deletes job results and Hypercube results

If a submission token is specified, text entries are also deleted. Requires admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import cleanup_api
from gams_engine.model.bad_input import BadInput
from gams_engine.model.message import Message
from gams_engine.model.not_found import NotFound
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cleanup_api.CleanupApi(api_client)
    filename = [
        "filename_example",
    ] # [str] |  (optional)
    token = [
        "token_example",
    ] # [str] |  (optional)
    hypercube_token = [
        "hypercube_token_example",
    ] # [str] |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Deletes job results and Hypercube results
        api_response = api_instance.remove_results(filename=filename, token=token, hypercube_token=hypercube_token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling CleanupApi->remove_results: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | **[str]**|  | [optional]
 **token** | **[str]**|  | [optional]
 **hypercube_token** | **[str]**|  | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Request |  -  |
**403** | Unauthorized access |  -  |
**404** | Some files are not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

