# gams_engine.DefaultApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_configuration**](DefaultApi.md#get_configuration) | **GET** /configuration | 
[**get_version**](DefaultApi.md#get_version) | **GET** /version | Returns Engine and GAMS versions
[**update_configuration**](DefaultApi.md#update_configuration) | **PATCH** /configuration | Updates configuration


# **get_configuration**
> ModelConfiguration get_configuration()



### Example


```python
import time
import gams_engine
from gams_engine.api import default_api
from gams_engine.model.model_configuration import ModelConfiguration
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with gams_engine.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        api_response = api_instance.get_configuration()
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling DefaultApi->get_configuration: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**ModelConfiguration**](ModelConfiguration.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_version**
> ModelVersion get_version()

Returns Engine and GAMS versions

### Example


```python
import time
import gams_engine
from gams_engine.api import default_api
from gams_engine.model.model_version import ModelVersion
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with gams_engine.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Returns Engine and GAMS versions
        api_response = api_instance.get_version()
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling DefaultApi->get_version: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**ModelVersion**](ModelVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_configuration**
> Message update_configuration()

Updates configuration

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import default_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)
    webhook_access = "DISABLED" # str | Whether to enable webhook access or enable it for admins only. Possible values are: ['DISABLED', 'ADMIN_ONLY', 'ENABLED'] (optional)
    text_entries_max_size = 0 # int | Maximum size (in bytes) for text entries. Text entries that exceed this size will be truncated. (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Updates configuration
        api_response = api_instance.update_configuration(webhook_access=webhook_access, text_entries_max_size=text_entries_max_size)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling DefaultApi->update_configuration: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhook_access** | **str**| Whether to enable webhook access or enable it for admins only. Possible values are: [&#39;DISABLED&#39;, &#39;ADMIN_ONLY&#39;, &#39;ENABLED&#39;] | [optional]
 **text_entries_max_size** | **int**| Maximum size (in bytes) for text entries. Text entries that exceed this size will be truncated. | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Request |  -  |
**403** | Unauthorized |  -  |
**404** | Not Found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

