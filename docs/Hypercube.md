# Hypercube


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_groups** | **[str], none_type** |  | [optional] 
**arguments** | **[str]** |  | [optional] 
**dep_tokens** | **[str], none_type** |  | [optional] 
**finished** | **int** |  | [optional] 
**finished_at** | **datetime, none_type** |  | [optional] 
**is_data_provided** | **bool** |  | [optional] 
**is_temporary_model** | **bool** |  | [optional] 
**job_count** | **int** |  | [optional] 
**labels** | [**ModelJobLabels**](ModelJobLabels.md) |  | [optional] 
**model** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**result_exists** | **bool, none_type** |  | [optional] 
**status** | **int** |  | [optional] 
**submitted_at** | **datetime** |  | [optional] 
**successfully_finished** | **int** |  | [optional] 
**tag** | **str, none_type** |  | [optional] 
**token** | **str** |  | [optional] 
**user** | [**ResultUser**](ResultUser.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


