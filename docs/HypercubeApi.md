# gams_engine.HypercubeApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_hypercube**](HypercubeApi.md#create_hypercube) | **POST** /hypercube/ | Posts a Hypercube job
[**delete_hypercube_zip**](HypercubeApi.md#delete_hypercube_zip) | **DELETE** /hypercube/{hypercube_token}/result | Deletes the results of the Hypercube job
[**get_hypercube_zip**](HypercubeApi.md#get_hypercube_zip) | **GET** /hypercube/{hypercube_token}/result | Downloads Hypercube job result
[**get_hypercube_zip_info**](HypercubeApi.md#get_hypercube_zip_info) | **HEAD** /hypercube/{hypercube_token}/result | Gets md5 hash and file size information of Hypercube job result
[**kill_hypercube**](HypercubeApi.md#kill_hypercube) | **DELETE** /hypercube/{hypercube_token} | Terminates the unfinished jobs that belong to a Hypercube job
[**list_hypercubes**](HypercubeApi.md#list_hypercubes) | **GET** /hypercube/ | Lists the Hypercube jobs sent by the user unless &#x60;everyone&#x60; flag is set
[**update_hypercube_access_groups**](HypercubeApi.md#update_hypercube_access_groups) | **PUT** /hypercube/{hypercube_token}/access-groups | Update access groups that can access a Hypercube job
[**update_hypercube_tag**](HypercubeApi.md#update_hypercube_tag) | **PUT** /hypercube/{hypercube_token}/tag | Update human-readable tag of a Hypercube job


# **create_hypercube**
> HypercubeToken create_hypercube(model, namespace, hypercube_file)

Posts a Hypercube job

If the model is a registered model, execute permission for the namespace is required. If model is a temporary model execute and write permission for the namespace is required. When the disk or volume quota reaches 80%, quota_warning is included in the response.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import hypercube_api
from gams_engine.model.message import Message
from gams_engine.model.quota_exceeded import QuotaExceeded
from gams_engine.model.hypercube_token import HypercubeToken
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = hypercube_api.HypercubeApi(api_client)
    model = "model_example" # str | Name of the model
    namespace = "namespace_example" # str | Namespace containing(or will contain) the model
    hypercube_file = open('/path/to/file', 'rb') # file_type | Hypercube description file
    run = "run_example" # str | Name of the main gms file with its extension. Will use model + '.gms' if not provided. (optional)
    inex_string = "inex_string_example" # str | Optional JSON string to filter the contents of the result zip file (inex_file takes precedence if specified) (optional)
    arguments = [
        "arguments_example",
    ] # [str] | Arguments that will be passed to GAMS call (optional)
    dep_tokens = [
        "dep_tokens_example",
    ] # [str] | Tokens of jobs on which this job depends. The order defines the order in which the results of dependent jobs are extracted. (optional)
    labels = [
        "labels_example",
    ] # [str] | Labels that will be attached to the job in key=value. Currently supported labels are: cpu_request, memory_request, workspace_request, node_selectors, tolerations, instance (optional)
    tag = "tag_example" # str | Human-readable tag to assign to job (at most 255 characters) (optional)
    access_groups = [
        "access_groups_example",
    ] # [str] | Labels of user groups that should be able to access this job. (optional)
    stdout_filename = "stdout_filename_example" # str |  (optional)
    model_data = open('/path/to/file', 'rb') # file_type | Zip file containing model files, if model is not registered (optional)
    data = open('/path/to/file', 'rb') # file_type | File containing data in zip (optional)
    inex_file = open('/path/to/file', 'rb') # file_type | Optional JSON file to filter the contents of the result zip file (optional)

    # example passing only required values which don't have defaults set
    try:
        # Posts a Hypercube job
        api_response = api_instance.create_hypercube(model, namespace, hypercube_file)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->create_hypercube: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Posts a Hypercube job
        api_response = api_instance.create_hypercube(model, namespace, hypercube_file, run=run, inex_string=inex_string, arguments=arguments, dep_tokens=dep_tokens, labels=labels, tag=tag, access_groups=access_groups, stdout_filename=stdout_filename, model_data=model_data, data=data, inex_file=inex_file)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->create_hypercube: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | **str**| Name of the model |
 **namespace** | **str**| Namespace containing(or will contain) the model |
 **hypercube_file** | **file_type**| Hypercube description file |
 **run** | **str**| Name of the main gms file with its extension. Will use model + &#39;.gms&#39; if not provided. | [optional]
 **inex_string** | **str**| Optional JSON string to filter the contents of the result zip file (inex_file takes precedence if specified) | [optional]
 **arguments** | **[str]**| Arguments that will be passed to GAMS call | [optional]
 **dep_tokens** | **[str]**| Tokens of jobs on which this job depends. The order defines the order in which the results of dependent jobs are extracted. | [optional]
 **labels** | **[str]**| Labels that will be attached to the job in key&#x3D;value. Currently supported labels are: cpu_request, memory_request, workspace_request, node_selectors, tolerations, instance | [optional]
 **tag** | **str**| Human-readable tag to assign to job (at most 255 characters) | [optional]
 **access_groups** | **[str]**| Labels of user groups that should be able to access this job. | [optional]
 **stdout_filename** | **str**|  | [optional]
 **model_data** | **file_type**| Zip file containing model files, if model is not registered | [optional]
 **data** | **file_type**| File containing data in zip | [optional]
 **inex_file** | **file_type**| Optional JSON file to filter the contents of the result zip file | [optional]

### Return type

[**HypercubeToken**](HypercubeToken.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful |  -  |
**400** | Bad Input |  -  |
**402** | Quota exceeded |  -  |
**403** | Unauthorized access |  -  |
**404** | Model or namespace not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_hypercube_zip**
> Message delete_hypercube_zip(hypercube_token)

Deletes the results of the Hypercube job

Job must belong to the logged in user, an invitee of the logged in user, or logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import hypercube_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = hypercube_api.HypercubeApi(api_client)
    hypercube_token = "hypercube_token_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Deletes the results of the Hypercube job
        api_response = api_instance.delete_hypercube_zip(hypercube_token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->delete_hypercube_zip: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hypercube_token** | **str**|  |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad request |  -  |
**403** | Unauthorized access |  -  |
**404** | Result not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_hypercube_zip**
> file_type get_hypercube_zip(hypercube_token)

Downloads Hypercube job result

The job must belong to the logged in user, an invitee of the logged in user, or logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import hypercube_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = hypercube_api.HypercubeApi(api_client)
    hypercube_token = "hypercube_token_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Downloads Hypercube job result
        api_response = api_instance.get_hypercube_zip(hypercube_token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->get_hypercube_zip: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hypercube_token** | **str**|  |

### Return type

**file_type**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/zip


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**403** | Unauthorized access |  -  |
**404** | Result not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_hypercube_zip_info**
> get_hypercube_zip_info(hypercube_token)

Gets md5 hash and file size information of Hypercube job result

The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import hypercube_api
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = hypercube_api.HypercubeApi(api_client)
    hypercube_token = "hypercube_token_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Gets md5 hash and file size information of Hypercube job result
        api_instance.get_hypercube_zip_info(hypercube_token)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->get_hypercube_zip_info: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hypercube_token** | **str**|  |

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  * st_size - Size of the file in bytes <br>  * md5_hash - MD5 hash of file <br>  |
**403** | Unauthorized access / File not found |  -  |
**404** | No job found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **kill_hypercube**
> Message kill_hypercube(hypercube_token)

Terminates the unfinished jobs that belong to a Hypercube job

Job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import hypercube_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = hypercube_api.HypercubeApi(api_client)
    hypercube_token = "hypercube_token_example" # str | 
    hard_kill = False # bool | Sends SIGKILL if true, SIGINT otherwise (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    try:
        # Terminates the unfinished jobs that belong to a Hypercube job
        api_response = api_instance.kill_hypercube(hypercube_token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->kill_hypercube: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Terminates the unfinished jobs that belong to a Hypercube job
        api_response = api_instance.kill_hypercube(hypercube_token, hard_kill=hard_kill)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->kill_hypercube: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hypercube_token** | **str**|  |
 **hard_kill** | **bool**| Sends SIGKILL if true, SIGINT otherwise | [optional] if omitted the server will use the default value of False

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Already cancelled |  -  |
**403** | Unauthorized |  -  |
**404** | Hypercube not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_hypercubes**
> HypercubePage list_hypercubes()

Lists the Hypercube jobs sent by the user unless `everyone` flag is set

If user has admin role and `everyone` flag is set, all Hypercube jobs are listed. If user is not inviter or admin and `everyone` flag is set, all visible Hypercube jobs (Hypercube jobs that were assigned to a user group that user is member of) are listed. If user is inviter and 'everyone' flag is set, Hypercube jobs of all invitees will be listed additionally.  If `page` is not one and there are no elements at that page, throws 404. Due to performance issues the fields `result_exists`, `dep_tokens`, `labels` and `access_groups` are only provided for queries for a single Hypercube job.  If `show_only_active` flag is set it only shows hypercube jobs that are not finished. `labels.resource_warning`, `labels.instance`, `labels.multiplier`, `access_groups` and `tag` fields are hidden by default for compatibility reasons, please use X-Fields header to get it. For example: X-Fields: \\*, labels{\\*}

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import hypercube_api
from gams_engine.model.message import Message
from gams_engine.model.hypercube_page import HypercubePage
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = hypercube_api.HypercubeApi(api_client)
    hypercube_token = "hypercube_token_example" # str | Hypercube token to filter (optional)
    everyone = True # bool | Can be set by admin/inviter; shows Hypercube submissions of everyone/invitees (optional)
    page = 1 # int |  (optional) if omitted the server will use the default value of 1
    per_page = 0 # int |  (optional) if omitted the server will use the default value of 0
    x_fields = "X-Fields_example" # str |  (optional)
    order_by = "submitted_at" # str |  (optional) if omitted the server will use the default value of "submitted_at"
    order_asc = False # bool |  (optional) if omitted the server will use the default value of False
    show_only_active = False # bool |  (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Lists the Hypercube jobs sent by the user unless `everyone` flag is set
        api_response = api_instance.list_hypercubes(hypercube_token=hypercube_token, everyone=everyone, page=page, per_page=per_page, x_fields=x_fields, order_by=order_by, order_asc=order_asc, show_only_active=show_only_active)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->list_hypercubes: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hypercube_token** | **str**| Hypercube token to filter | [optional]
 **everyone** | **bool**| Can be set by admin/inviter; shows Hypercube submissions of everyone/invitees | [optional]
 **page** | **int**|  | [optional] if omitted the server will use the default value of 1
 **per_page** | **int**|  | [optional] if omitted the server will use the default value of 0
 **x_fields** | **str**|  | [optional]
 **order_by** | **str**|  | [optional] if omitted the server will use the default value of "submitted_at"
 **order_asc** | **bool**|  | [optional] if omitted the server will use the default value of False
 **show_only_active** | **bool**|  | [optional] if omitted the server will use the default value of False

### Return type

[**HypercubePage**](HypercubePage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | Page not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_hypercube_access_groups**
> Message update_hypercube_access_groups(hypercube_token)

Update access groups that can access a Hypercube job

Can be queried via listHypercubes endpoint.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import hypercube_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = hypercube_api.HypercubeApi(api_client)
    hypercube_token = "hypercube_token_example" # str | 
    access_groups = [
        "access_groups_example",
    ] # [str] | Labels of user groups that should be able to access this job. (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update access groups that can access a Hypercube job
        api_response = api_instance.update_hypercube_access_groups(hypercube_token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->update_hypercube_access_groups: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update access groups that can access a Hypercube job
        api_response = api_instance.update_hypercube_access_groups(hypercube_token, access_groups=access_groups)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->update_hypercube_access_groups: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hypercube_token** | **str**|  |
 **access_groups** | **[str]**| Labels of user groups that should be able to access this job. | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Job not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_hypercube_tag**
> Message update_hypercube_tag(hypercube_token, tag)

Update human-readable tag of a Hypercube job

Stored in `tag` field. Can be queried via listHypercubes endpoint.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import hypercube_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = hypercube_api.HypercubeApi(api_client)
    hypercube_token = "hypercube_token_example" # str | 
    tag = "jUR,rZ#UM/?R,Fp^l6$ARjbhJk C>i H'qT\{<?'es#)#iK.YM{Rag2/!KB!k@5oXh.:Ts";mGL,i&z5[P@M"lzfB+Y,Twzfu~N^z"mfqecVU{SmA{QA<Y8XX0<}J;Krm9W'g~?)DvDDLE7-'(u+-7Tfp&\`F+7-?{%@=iEPLVY*a@A[b_6cfy~~0Gc" # str | Human-readable tag to assign to job (at most 255 characters)

    # example passing only required values which don't have defaults set
    try:
        # Update human-readable tag of a Hypercube job
        api_response = api_instance.update_hypercube_tag(hypercube_token, tag)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling HypercubeApi->update_hypercube_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hypercube_token** | **str**|  |
 **tag** | **str**| Human-readable tag to assign to job (at most 255 characters) |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Job not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

