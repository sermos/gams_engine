# Invitation


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | **datetime** |  | [optional] 
**gams_license** | **str, none_type** |  | [optional] 
**inviter_name** | **str** |  | [optional] 
**permissions** | **[str]** |  | [optional] 
**quota** | [**InvitationQuota**](InvitationQuota.md) |  | [optional] 
**roles** | **[str]** |  | [optional] 
**token** | **str** |  | [optional] 
**used** | **bool** |  | [optional] 
**user_groups** | **[str]** |  | [optional] 
**username** | **str, none_type** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


