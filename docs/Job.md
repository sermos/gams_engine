# Job


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_groups** | **[str]** |  | [optional] 
**arguments** | **[str]** |  | [optional] 
**dep_tokens** | **[str]** |  | [optional] 
**finished_at** | **datetime, none_type** |  | [optional] 
**is_data_provided** | **bool** |  | [optional] 
**is_temporary_model** | **bool** |  | [optional] 
**labels** | [**ModelJobLabels**](ModelJobLabels.md) |  | [optional] 
**model** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**process_status** | **int, none_type** |  | [optional] 
**result_exists** | **bool** |  | [optional] 
**status** | **int** |  | [optional] 
**stdout_filename** | **str** |  | [optional] 
**stream_entries** | **[str]** |  | [optional] 
**submitted_at** | **datetime** |  | [optional] 
**tag** | **str, none_type** |  | [optional] 
**text_entries** | [**[TextEntry]**](TextEntry.md) |  | [optional] 
**token** | **str** |  | [optional] 
**user** | [**ResultUser**](ResultUser.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


