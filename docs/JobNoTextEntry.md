# JobNoTextEntry


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**arguments** | **[str]** |  | [optional] 
**finished_at** | **datetime, none_type** |  | [optional] 
**is_data_provided** | **bool** |  | [optional] 
**is_temporary_model** | **bool** |  | [optional] 
**model** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**process_status** | **int, none_type** |  | [optional] 
**status** | **int** |  | [optional] 
**stdout_filename** | **str** |  | [optional] 
**stream_entries** | **[str]** |  | [optional] 
**submitted_at** | **datetime** |  | [optional] 
**tag** | **str, none_type** |  | [optional] 
**token** | **str** |  | [optional] 
**user** | [**ResultUser**](ResultUser.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


