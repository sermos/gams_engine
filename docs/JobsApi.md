# gams_engine.JobsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**consume_stream_entry**](JobsApi.md#consume_stream_entry) | **DELETE** /jobs/{token}/stream-entry | Flushes and returns the stream entry queue
[**create_job**](JobsApi.md#create_job) | **POST** /jobs/ | Submits a new job to be solved
[**delete_job_zip**](JobsApi.md#delete_job_zip) | **DELETE** /jobs/{token}/result | Deletes the job result and all the text entries
[**get_job**](JobsApi.md#get_job) | **GET** /jobs/{token} | Returns the details of a job
[**get_job_text_entry**](JobsApi.md#get_job_text_entry) | **GET** /jobs/{token}/text-entry/{entry_name} | Gets the value of a text entry
[**get_job_text_entry_info**](JobsApi.md#get_job_text_entry_info) | **HEAD** /jobs/{token}/text-entry/{entry_name} | Returns the number of characters of the text entry
[**get_job_zip**](JobsApi.md#get_job_zip) | **GET** /jobs/{token}/result | Downloads job result
[**get_job_zip_info**](JobsApi.md#get_job_zip_info) | **HEAD** /jobs/{token}/result | Gets size and md5 hash of job result
[**get_status_codes**](JobsApi.md#get_status_codes) | **GET** /jobs/status-codes | Returns mapping of job status codes to human readable messages
[**kill_job**](JobsApi.md#kill_job) | **DELETE** /jobs/{token} | Sends interrupt signal to a job
[**list_jobs**](JobsApi.md#list_jobs) | **GET** /jobs/ | Lists the jobs of the user unless &#x60;everyone&#x60; flag is set
[**pop_job_logs**](JobsApi.md#pop_job_logs) | **DELETE** /jobs/{token}/unread-logs | Flushes and returns stdout of job
[**pop_stream_entry**](JobsApi.md#pop_stream_entry) | **DELETE** /jobs/{token}/stream-entry/{entry_name} | Flushes and returns the stream entry queue
[**query_job_text_entry**](JobsApi.md#query_job_text_entry) | **GET** /jobs/{token}/text-entry | Gets the value of a text entry
[**query_job_text_entry_info**](JobsApi.md#query_job_text_entry_info) | **HEAD** /jobs/{token}/text-entry | Returns the number of characters of the text entry
[**update_job_access_groups**](JobsApi.md#update_job_access_groups) | **PUT** /jobs/{token}/access-groups | Update access groups that can access a job
[**update_job_tag**](JobsApi.md#update_job_tag) | **PUT** /jobs/{token}/tag | Update human-readable tag of a job


# **consume_stream_entry**
> StreamEntry consume_stream_entry(token, entry_name)

Flushes and returns the stream entry queue

The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from gams_engine.model.stream_entry import StreamEntry
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | Token of the job whose status is to be queried
    entry_name = "entry_name_example" # str | Name of the text entry to be queried

    # example passing only required values which don't have defaults set
    try:
        # Flushes and returns the stream entry queue
        api_response = api_instance.consume_stream_entry(token, entry_name)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->consume_stream_entry: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**| Token of the job whose status is to be queried |
 **entry_name** | **str**| Name of the text entry to be queried |

### Return type

[**StreamEntry**](StreamEntry.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**308** | Stream entry is gone |  -  |
**403** | Unauthorized access |  -  |
**404** | No job found / No stream entry found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_job**
> MessageAndToken create_job(model, namespace)

Submits a new job to be solved

Requires execute permission for registered models, requires write and execute permissions for unregistered models. When the disk or volume quota reaches 80%, quota_warning is included in the response.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from gams_engine.model.quota_exceeded import QuotaExceeded
from gams_engine.model.message_and_token import MessageAndToken
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    model = "model_example" # str | Name of the model
    namespace = "namespace_example" # str | Namespace containing(or will contain) the model
    run = "run_example" # str | Name of the main gms file with its extension. Will use model + '.gms' if not provided. (optional)
    inex_string = "inex_string_example" # str | Optional JSON string to filter the contents of the result zip file (inex_file takes precedence if specified) (optional)
    text_entries = [
        "text_entries_example",
    ] # [str] |  (optional)
    stream_entries = [
        "stream_entries_example",
    ] # [str] |  (optional)
    stdout_filename = "log_stdout.txt" # str | Name of the file that captures stdout (optional) if omitted the server will use the default value of "log_stdout.txt"
    arguments = [
        "arguments_example",
    ] # [str] | Arguments that will be passed to GAMS call (optional)
    dep_tokens = [
        "dep_tokens_example",
    ] # [str] | Tokens of jobs on which this job depends. The order defines the order in which the results of dependent jobs are extracted. (optional)
    labels = [
        "labels_example",
    ] # [str] | Labels that will be attached to the job in key=value. Currently supported labels are: cpu_request, memory_request, workspace_request, node_selectors, tolerations, instance (optional)
    tag = "tag_example" # str | Human-readable tag to assign to job (at most 255 characters) (optional)
    access_groups = [
        "access_groups_example",
    ] # [str] | Labels of user groups that should be able to access this job. (optional)
    model_data = open('/path/to/file', 'rb') # file_type | Zip file containing model files, if model is not registered (optional)
    data = open('/path/to/file', 'rb') # file_type | File containing data in zip (optional)
    inex_file = open('/path/to/file', 'rb') # file_type | Optional JSON file to filter the contents of the result zip file (optional)

    # example passing only required values which don't have defaults set
    try:
        # Submits a new job to be solved
        api_response = api_instance.create_job(model, namespace)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->create_job: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Submits a new job to be solved
        api_response = api_instance.create_job(model, namespace, run=run, inex_string=inex_string, text_entries=text_entries, stream_entries=stream_entries, stdout_filename=stdout_filename, arguments=arguments, dep_tokens=dep_tokens, labels=labels, tag=tag, access_groups=access_groups, model_data=model_data, data=data, inex_file=inex_file)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->create_job: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | **str**| Name of the model |
 **namespace** | **str**| Namespace containing(or will contain) the model |
 **run** | **str**| Name of the main gms file with its extension. Will use model + &#39;.gms&#39; if not provided. | [optional]
 **inex_string** | **str**| Optional JSON string to filter the contents of the result zip file (inex_file takes precedence if specified) | [optional]
 **text_entries** | **[str]**|  | [optional]
 **stream_entries** | **[str]**|  | [optional]
 **stdout_filename** | **str**| Name of the file that captures stdout | [optional] if omitted the server will use the default value of "log_stdout.txt"
 **arguments** | **[str]**| Arguments that will be passed to GAMS call | [optional]
 **dep_tokens** | **[str]**| Tokens of jobs on which this job depends. The order defines the order in which the results of dependent jobs are extracted. | [optional]
 **labels** | **[str]**| Labels that will be attached to the job in key&#x3D;value. Currently supported labels are: cpu_request, memory_request, workspace_request, node_selectors, tolerations, instance | [optional]
 **tag** | **str**| Human-readable tag to assign to job (at most 255 characters) | [optional]
 **access_groups** | **[str]**| Labels of user groups that should be able to access this job. | [optional]
 **model_data** | **file_type**| Zip file containing model files, if model is not registered | [optional]
 **data** | **file_type**| File containing data in zip | [optional]
 **inex_file** | **file_type**| Optional JSON file to filter the contents of the result zip file | [optional]

### Return type

[**MessageAndToken**](MessageAndToken.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Job Created |  -  |
**400** | Input is not valid |  -  |
**401** | Invalid authentication |  -  |
**402** | Quota exceeded |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace could not be found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_job_zip**
> Message delete_job_zip(token)

Deletes the job result and all the text entries

The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Deletes the job result and all the text entries
        api_response = api_instance.delete_job_zip(token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->delete_job_zip: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**|  |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |
**403** | Job data does not exist |  -  |
**404** | No job found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job**
> Job get_job(token)

Returns the details of a job

The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role. `labels.resource_warning`, `labels.instance`, `labels.multiplier`, `access_groups` and `tag` fields are hidden by default for compatibility reasons, please use X-Fields header to get it. For example: X-Fields: \\*, labels{\\*}

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from gams_engine.model.job import Job
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | 
    x_fields = "X-Fields_example" # str |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Returns the details of a job
        api_response = api_instance.get_job(token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->get_job: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Returns the details of a job
        api_response = api_instance.get_job(token, x_fields=x_fields)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->get_job: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**|  |
 **x_fields** | **str**|  | [optional]

### Return type

[**Job**](Job.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Succesful |  -  |
**403** | Unauthorized access |  -  |
**404** | Job not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_text_entry**
> TextEntry get_job_text_entry(token, entry_name)

Gets the value of a text entry

Start position and length can be specified to get a partial text entry. The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.text_entry import TextEntry
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | Token of the job whose status is to be queried
    entry_name = "entry_name_example" # str | Name of the text entry to be queried
    start_position = 1 # int | Start position of the substring (optional) if omitted the server will use the default value of 1
    length = 1 # int | Number of chars you want to extract; if omitted, whole string is extracted (optional)

    # example passing only required values which don't have defaults set
    try:
        # Gets the value of a text entry
        api_response = api_instance.get_job_text_entry(token, entry_name)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->get_job_text_entry: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Gets the value of a text entry
        api_response = api_instance.get_job_text_entry(token, entry_name, start_position=start_position, length=length)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->get_job_text_entry: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**| Token of the job whose status is to be queried |
 **entry_name** | **str**| Name of the text entry to be queried |
 **start_position** | **int**| Start position of the substring | [optional] if omitted the server will use the default value of 1
 **length** | **int**| Number of chars you want to extract; if omitted, whole string is extracted | [optional]

### Return type

[**TextEntry**](TextEntry.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad input |  -  |
**403** | Unauthorized access |  -  |
**404** | No job found / The text entry cannot be found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_text_entry_info**
> get_job_text_entry_info(token, entry_name)

Returns the number of characters of the text entry

The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | Token of the job whose status is to be queried
    entry_name = "entry_name_example" # str | Name of the text entry to be queried

    # example passing only required values which don't have defaults set
    try:
        # Returns the number of characters of the text entry
        api_instance.get_job_text_entry_info(token, entry_name)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->get_job_text_entry_info: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**| Token of the job whose status is to be queried |
 **entry_name** | **str**| Name of the text entry to be queried |

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  * char_length - an integer will be here <br>  |
**403** | Unauthorized access |  -  |
**404** | No job found / The text entry cannot be found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_zip**
> file_type get_job_zip(token)

Downloads job result

The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Downloads job result
        api_response = api_instance.get_job_zip(token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->get_job_zip: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**|  |

### Return type

**file_type**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/zip


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  * content-disposition - attachment; filename&#x3D;model.zip <br>  * content-type - application/zip <br>  |
**403** | Job data does not exist |  -  |
**404** | No job found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_job_zip_info**
> get_job_zip_info(token)

Gets size and md5 hash of job result

The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Gets size and md5 hash of job result
        api_instance.get_job_zip_info(token)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->get_job_zip_info: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**|  |

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  * st_size - Size of the file in bytes <br>  * md5_hash - MD5 hash of file <br>  |
**403** | Unauthorized access |  -  |
**404** | Job or file not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_status_codes**
> [StatusCodeMeaning] get_status_codes()

Returns mapping of job status codes to human readable messages

### Example


```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.status_code_meaning import StatusCodeMeaning
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with gams_engine.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    x_fields = "X-Fields_example" # str | An optional fields mask (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Returns mapping of job status codes to human readable messages
        api_response = api_instance.get_status_codes(x_fields=x_fields)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->get_status_codes: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fields** | **str**| An optional fields mask | [optional]

### Return type

[**[StatusCodeMeaning]**](StatusCodeMeaning.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **kill_job**
> Message kill_job(token)

Sends interrupt signal to a job

It has no effect if the job is finished. If the job is pending, worker will disregard the job when it receives the job. If the job is running, it will be terminated. The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | 
    hard_kill = False # bool | Sends SIGKILL if true, SIGINT otherwise (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    try:
        # Sends interrupt signal to a job
        api_response = api_instance.kill_job(token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->kill_job: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Sends interrupt signal to a job
        api_response = api_instance.kill_job(token, hard_kill=hard_kill)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->kill_job: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**|  |
 **hard_kill** | **bool**| Sends SIGKILL if true, SIGINT otherwise | [optional] if omitted the server will use the default value of False

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Job is already finished |  -  |
**403** | Unauthorized access |  -  |
**404** | Job not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_jobs**
> JobNoTextEntryPage list_jobs()

Lists the jobs of the user unless `everyone` flag is set

If user has admin role and `everyone` flag is set, all jobs are listed. If user is not inviter or admin and `everyone` flag is set, all visible jobs (jobs that were assigned to a user group that user is member of) are listed. If user is inviter and 'everyone' flag is set, jobs of all invitees will be listed additionally.  If `page` is not one and there are no elements at that page, throws 404.  `per_page` is zero by default and it indicates that you want all the values. If a non-zero value provided to `per_page`, result will be paginated by that `page_size`  if `show_only_active` set, submissions that are waiting, queued, running or cancelling will be returned.  The fields that are displayed by default are: `model`, `status`, `process_status`, `submitted_at`, `stdout_filename`, `namespace`, `token`, `finished_at` To see other fields, please use X-Fields accordingly. E.g `X-Fields: *` will show everything.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from gams_engine.model.job_no_text_entry_page import JobNoTextEntryPage
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    everyone = True # bool | Can be set by admin/inviter; shows submissions of everyone/invitees (optional)
    x_fields = "X-Fields_example" # str |  (optional)
    page = 1 # int |  (optional) if omitted the server will use the default value of 1
    per_page = 0 # int |  (optional) if omitted the server will use the default value of 0
    order_by = "submitted_at" # str |  (optional) if omitted the server will use the default value of "submitted_at"
    order_asc = False # bool |  (optional) if omitted the server will use the default value of False
    show_only_active = False # bool |  (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Lists the jobs of the user unless `everyone` flag is set
        api_response = api_instance.list_jobs(everyone=everyone, x_fields=x_fields, page=page, per_page=per_page, order_by=order_by, order_asc=order_asc, show_only_active=show_only_active)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->list_jobs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **everyone** | **bool**| Can be set by admin/inviter; shows submissions of everyone/invitees | [optional]
 **x_fields** | **str**|  | [optional]
 **page** | **int**|  | [optional] if omitted the server will use the default value of 1
 **per_page** | **int**|  | [optional] if omitted the server will use the default value of 0
 **order_by** | **str**|  | [optional] if omitted the server will use the default value of "submitted_at"
 **order_asc** | **bool**|  | [optional] if omitted the server will use the default value of False
 **show_only_active** | **bool**|  | [optional] if omitted the server will use the default value of False

### Return type

[**JobNoTextEntryPage**](JobNoTextEntryPage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | Page not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pop_job_logs**
> LogPiece pop_job_logs(token)

Flushes and returns stdout of job

The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from gams_engine.model.log_piece import LogPiece
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Flushes and returns stdout of job
        api_response = api_instance.pop_job_logs(token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->pop_job_logs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**|  |

### Return type

[**LogPiece**](LogPiece.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**308** | Partial log is not available, get the full log |  -  |
**403** | Cannot get logs of pending job / Unauthorized access |  -  |
**404** | No job found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pop_stream_entry**
> StreamEntry pop_stream_entry(token, entry_name)

Flushes and returns the stream entry queue

The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from gams_engine.model.stream_entry import StreamEntry
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | Token of the job whose status is to be queried
    entry_name = "entry_name_example" # str | Name of the stream entry to be queried

    # example passing only required values which don't have defaults set
    try:
        # Flushes and returns the stream entry queue
        api_response = api_instance.pop_stream_entry(token, entry_name)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->pop_stream_entry: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**| Token of the job whose status is to be queried |
 **entry_name** | **str**| Name of the stream entry to be queried |

### Return type

[**StreamEntry**](StreamEntry.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**308** | Stream entry is gone |  -  |
**403** | Unauthorized access |  -  |
**404** | No job found / No stream entry found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **query_job_text_entry**
> TextEntry query_job_text_entry(token, entry_name)

Gets the value of a text entry

Start position and length can be specified to get a partial text entry. The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.text_entry import TextEntry
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | Token of the job whose status is to be queried
    entry_name = "entry_name_example" # str | Name of the text entry to be queried
    start_position = 1 # int | Start position of the substring (optional) if omitted the server will use the default value of 1
    length = 1 # int | Number of chars you want to extract; if omitted, whole string is extracted (optional)

    # example passing only required values which don't have defaults set
    try:
        # Gets the value of a text entry
        api_response = api_instance.query_job_text_entry(token, entry_name)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->query_job_text_entry: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Gets the value of a text entry
        api_response = api_instance.query_job_text_entry(token, entry_name, start_position=start_position, length=length)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->query_job_text_entry: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**| Token of the job whose status is to be queried |
 **entry_name** | **str**| Name of the text entry to be queried |
 **start_position** | **int**| Start position of the substring | [optional] if omitted the server will use the default value of 1
 **length** | **int**| Number of chars you want to extract; if omitted, whole string is extracted | [optional]

### Return type

[**TextEntry**](TextEntry.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad input |  -  |
**403** | Unauthorized access |  -  |
**404** | No job found / The text entry cannot be found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **query_job_text_entry_info**
> query_job_text_entry_info(token, entry_name)

Returns the number of characters of the text entry

The job must belong to the logged in user, an invitee of the logged in user, or the logged in user must have admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | Token of the job whose status is to be queried
    entry_name = "entry_name_example" # str | Name of the text entry to be queried

    # example passing only required values which don't have defaults set
    try:
        # Returns the number of characters of the text entry
        api_instance.query_job_text_entry_info(token, entry_name)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->query_job_text_entry_info: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**| Token of the job whose status is to be queried |
 **entry_name** | **str**| Name of the text entry to be queried |

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  * char_length - an integer will be here <br>  |
**403** | Unauthorized access |  -  |
**404** | No job found / The text entry cannot be found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_job_access_groups**
> Message update_job_access_groups(token)

Update access groups that can access a job

Can be queried via getJob endpoint.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | 
    access_groups = [
        "access_groups_example",
    ] # [str] | Labels of user groups that should be able to access this job. (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update access groups that can access a job
        api_response = api_instance.update_job_access_groups(token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->update_job_access_groups: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update access groups that can access a job
        api_response = api_instance.update_job_access_groups(token, access_groups=access_groups)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->update_job_access_groups: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**|  |
 **access_groups** | **[str]**| Labels of user groups that should be able to access this job. | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Job not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_job_tag**
> Message update_job_tag(token, tag)

Update human-readable tag of a job

Stored in `tag` field. Can be queried via getJob endpoint.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import jobs_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jobs_api.JobsApi(api_client)
    token = "token_example" # str | 
    tag = "jUR,rZ#UM/?R,Fp^l6$ARjbhJk C>i H'qT\{<?'es#)#iK.YM{Rag2/!KB!k@5oXh.:Ts";mGL,i&z5[P@M"lzfB+Y,Twzfu~N^z"mfqecVU{SmA{QA<Y8XX0<}J;Krm9W'g~?)DvDDLE7-'(u+-7Tfp&\`F+7-?{%@=iEPLVY*a@A[b_6cfy~~0Gc" # str | Human-readable tag to assign to job (at most 255 characters)

    # example passing only required values which don't have defaults set
    try:
        # Update human-readable tag of a job
        api_response = api_instance.update_job_tag(token, tag)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling JobsApi->update_job_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**|  |
 **tag** | **str**| Human-readable tag to assign to job (at most 255 characters) |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Invalid input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Job not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

