# gams_engine.LicensesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_license**](LicensesApi.md#delete_license) | **DELETE** /licenses/ | Deletes the license associated with the given user
[**delete_system_wide_gams_license**](LicensesApi.md#delete_system_wide_gams_license) | **DELETE** /licenses/system-wide | Deletes the system-wide GAMS license (only admins)
[**get_engine_license**](LicensesApi.md#get_engine_license) | **GET** /licenses/engine | Returns Engine license
[**get_license**](LicensesApi.md#get_license) | **GET** /licenses/ | Lists the users&#39; licenses
[**get_system_wide_gams_license**](LicensesApi.md#get_system_wide_gams_license) | **GET** /licenses/system-wide | Fetches the system-wide GAMS license (only admins)
[**update_engine_license**](LicensesApi.md#update_engine_license) | **PUT** /licenses/engine | Updates Engine license
[**update_license**](LicensesApi.md#update_license) | **PUT** /licenses/ | Associates a license with a user
[**update_system_wide_gams_license**](LicensesApi.md#update_system_wide_gams_license) | **PUT** /licenses/system-wide | Updates the system-wide GAMS license (only admins)


# **delete_license**
> Message delete_license(username)

Deletes the license associated with the given user

Deletes licenses from invited users if the affected user is an inviter. The user must be the owner of the license, not just an heir. Requires admin privileges.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import licenses_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = licenses_api.LicensesApi(api_client)
    username = "username_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Deletes the license associated with the given user
        api_response = api_instance.delete_license(username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling LicensesApi->delete_license: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**|  |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Request |  -  |
**403** | Unauthorized |  -  |
**404** | Not Found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_system_wide_gams_license**
> Message delete_system_wide_gams_license()

Deletes the system-wide GAMS license (only admins)

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import licenses_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = licenses_api.LicensesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Deletes the system-wide GAMS license (only admins)
        api_response = api_instance.delete_system_wide_gams_license()
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling LicensesApi->delete_system_wide_gams_license: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**403** | Unauthorized |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_engine_license**
> EngineLicense get_engine_license()

Returns Engine license

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import licenses_api
from gams_engine.model.message import Message
from gams_engine.model.engine_license import EngineLicense
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = licenses_api.LicensesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Returns Engine license
        api_response = api_instance.get_engine_license()
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling LicensesApi->get_engine_license: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**EngineLicense**](EngineLicense.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**403** | Unauthorized access |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_license**
> [License] get_license()

Lists the users' licenses

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import licenses_api
from gams_engine.model.license import License
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = licenses_api.LicensesApi(api_client)
    username = "username_example" # str | Username of the user to filter (optional)
    x_fields = "X-Fields_example" # str |  (optional)
    only_owners = False # bool | Show only license owners, not the heirs (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Lists the users' licenses
        api_response = api_instance.get_license(username=username, x_fields=x_fields, only_owners=only_owners)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling LicensesApi->get_license: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| Username of the user to filter | [optional]
 **x_fields** | **str**|  | [optional]
 **only_owners** | **bool**| Show only license owners, not the heirs | [optional] if omitted the server will use the default value of False

### Return type

[**[License]**](License.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_system_wide_gams_license**
> SystemWideLicense get_system_wide_gams_license()

Fetches the system-wide GAMS license (only admins)

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import licenses_api
from gams_engine.model.system_wide_license import SystemWideLicense
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = licenses_api.LicensesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Fetches the system-wide GAMS license (only admins)
        api_response = api_instance.get_system_wide_gams_license()
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling LicensesApi->get_system_wide_gams_license: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**SystemWideLicense**](SystemWideLicense.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Request |  -  |
**403** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_engine_license**
> Message update_engine_license(license)

Updates Engine license

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import licenses_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = licenses_api.LicensesApi(api_client)
    license = "license_example" # str | Engine license

    # example passing only required values which don't have defaults set
    try:
        # Updates Engine license
        api_response = api_instance.update_engine_license(license)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling LicensesApi->update_engine_license: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **license** | **str**| Engine license |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Input is not valid |  -  |
**403** | Unauthorized access |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_license**
> Message update_license(username, license)

Associates a license with a user

Overwrites the user's current license, if any. When inviters receive a license, they pass the license on to the users they have invited. Requires admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import licenses_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = licenses_api.LicensesApi(api_client)
    username = "username_example" # str | 
    license = "license_example" # str | Base64 encoded License

    # example passing only required values which don't have defaults set
    try:
        # Associates a license with a user
        api_response = api_instance.update_license(username, license)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling LicensesApi->update_license: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**|  |
 **license** | **str**| Base64 encoded License |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Request |  -  |
**403** | Unauthorized |  -  |
**404** | Not Found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_system_wide_gams_license**
> Message update_system_wide_gams_license(license)

Updates the system-wide GAMS license (only admins)

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import licenses_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = licenses_api.LicensesApi(api_client)
    license = "license_example" # str | Base64 encoded system-wide GAMS License

    # example passing only required values which don't have defaults set
    try:
        # Updates the system-wide GAMS license (only admins)
        api_response = api_instance.update_system_wide_gams_license(license)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling LicensesApi->update_system_wide_gams_license: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **license** | **str**| Base64 encoded system-wide GAMS License |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Request |  -  |
**403** | Unauthorized |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

