# ModelHypercubeJob


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_number** | **int** |  | [optional] 
**process_status** | **int, none_type** |  | [optional] 
**status** | **int** |  | [optional] 
**times** | [**[TimeSpan]**](TimeSpan.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


