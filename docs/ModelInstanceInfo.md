# ModelInstanceInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cpu_request** | **float** |  | [optional] 
**label** | **str** |  | [optional] 
**memory_request** | **int** |  | [optional] 
**multiplier** | **float** |  | [optional] 
**node_selectors** | [**[GenericKeyValuePair]**](GenericKeyValuePair.md) |  | [optional] 
**tolerations** | [**[GenericKeyValuePair]**](GenericKeyValuePair.md) |  | [optional] 
**workspace_request** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


