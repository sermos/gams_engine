# ModelJobLabels


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cpu_request** | **float, none_type** |  | [optional] 
**instance** | **str, none_type** |  | [optional] 
**memory_request** | **int, none_type** |  | [optional] 
**multiplier** | **float, none_type** |  | [optional] 
**node_selectors** | [**[GenericKeyValuePair], none_type**](GenericKeyValuePair.md) |  | [optional] 
**resource_warning** | **str, none_type** |  | [optional] 
**tolerations** | [**[GenericKeyValuePair], none_type**](GenericKeyValuePair.md) |  | [optional] 
**workspace_request** | **int, none_type** |  | [optional] 
**any string name** | **str** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


