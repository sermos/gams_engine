# ModelJobUsage


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**finished** | **datetime, none_type** |  | [optional] 
**labels** | [**ModelJobLabels**](ModelJobLabels.md) |  | [optional] 
**model** | **str** |  | [optional] 
**namespace** | **str** |  | [optional] 
**process_status** | **int, none_type** |  | [optional] 
**status** | **int** |  | [optional] 
**submitted** | **datetime** |  | [optional] 
**times** | [**[TimeSpan]**](TimeSpan.md) |  | [optional] 
**token** | **str** |  | [optional] 
**username** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


