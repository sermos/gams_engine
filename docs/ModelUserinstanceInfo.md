# ModelUserinstanceInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default_inherited_from** | **str, none_type** |  | [optional] 
**default_instance** | [**ModelInstanceInfo**](ModelInstanceInfo.md) |  | [optional] 
**instances_available** | [**[ModelInstanceInfo]**](ModelInstanceInfo.md) |  | [optional] 
**instances_inherited_from** | **str, none_type** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


