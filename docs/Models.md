# Models


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**arguments** | **[str]** |  | [optional] 
**inex** | [**Inex**](Inex.md) |  | [optional] 
**length** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**protect_model_files** | **bool** |  | [optional]  if omitted the server will use the default value of False
**run** | **str, none_type** |  | [optional] 
**stream_entries** | **[str]** |  | [optional] 
**text_entries** | **[str]** |  | [optional] 
**upload_date** | **datetime** |  | [optional] 
**user_groups** | **[str]** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


