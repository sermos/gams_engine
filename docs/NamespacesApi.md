# gams_engine.NamespacesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_user_to_group**](NamespacesApi.md#add_user_to_group) | **POST** /namespaces/{namespace}/user-groups/{label} | Adds a user to a group
[**create_model**](NamespacesApi.md#create_model) | **POST** /namespaces/{namespace}/models/{model} | Registers a new model in the given namespace
[**create_model_deprecated**](NamespacesApi.md#create_model_deprecated) | **POST** /namespaces/{namespace}/{model} | Registers a new model in the given namespace
[**create_namespace**](NamespacesApi.md#create_namespace) | **POST** /namespaces/{namespace} | Creates a namespace
[**create_user_group**](NamespacesApi.md#create_user_group) | **POST** /namespaces/{namespace}/user-groups | Creates a new user group
[**delete_model**](NamespacesApi.md#delete_model) | **DELETE** /namespaces/{namespace}/models/{model} | Deletes a model registered in the namespace
[**delete_model_deprecated**](NamespacesApi.md#delete_model_deprecated) | **DELETE** /namespaces/{namespace}/{model} | Deletes a model registered in the namespace
[**delete_namespace**](NamespacesApi.md#delete_namespace) | **DELETE** /namespaces/{namespace} | Deletes a namespace
[**delete_namespace_quota**](NamespacesApi.md#delete_namespace_quota) | **DELETE** /namespaces/{namespace}/disk-quota | Deletes namespace disk quota
[**delete_user_group**](NamespacesApi.md#delete_user_group) | **DELETE** /namespaces/{namespace}/user-groups | Deletes a user group
[**get_accessible_namespaces**](NamespacesApi.md#get_accessible_namespaces) | **GET** /namespaces/permissions/me | Lists the namespaces where the user has a non-zero permission, along with that &#x60;permission&#x60; and &#x60;disk_quota&#x60;
[**get_model**](NamespacesApi.md#get_model) | **GET** /namespaces/{namespace}/models/{model} | Downloads model data
[**get_model_deprecated**](NamespacesApi.md#get_model_deprecated) | **GET** /namespaces/{namespace}/{model} | Downloads model data
[**get_my_permissions**](NamespacesApi.md#get_my_permissions) | **GET** /namespaces/{namespace}/permissions/me | Returns the permissions of the logged in user in the given namespace
[**get_namespace_quota**](NamespacesApi.md#get_namespace_quota) | **GET** /namespaces/{namespace}/disk-quota | Displays namespace disk quota
[**get_user_groups**](NamespacesApi.md#get_user_groups) | **GET** /namespaces/{namespace}/user-groups | Fetches user groups
[**get_user_permission**](NamespacesApi.md#get_user_permission) | **GET** /namespaces/{namespace}/permissions | Gets permissions of the given user for the given namespace
[**list_models**](NamespacesApi.md#list_models) | **GET** /namespaces/{namespace} | Lists the models in a namespace
[**list_namespaces**](NamespacesApi.md#list_namespaces) | **GET** /namespaces/ | Lists the namespaces
[**remove_user_from_group**](NamespacesApi.md#remove_user_from_group) | **DELETE** /namespaces/{namespace}/user-groups/{label} | Removes a user from a group
[**replace_user_permission**](NamespacesApi.md#replace_user_permission) | **PUT** /namespaces/{namespace}/permissions | Sets permissions of the given user for the given namespace
[**update_model**](NamespacesApi.md#update_model) | **PATCH** /namespaces/{namespace}/models/{model} | Partially update registered models
[**update_model_deprecated**](NamespacesApi.md#update_model_deprecated) | **PATCH** /namespaces/{namespace}/{model} | Partially update registered models
[**update_namespace_quota**](NamespacesApi.md#update_namespace_quota) | **PUT** /namespaces/{namespace}/disk-quota | Updates namespace disk quota


# **add_user_to_group**
> Message add_user_to_group(namespace, label, username)

Adds a user to a group

Inviters can add any invitees to a group they are a member of or that belongs to an invitee. Admins can add anyone to any group.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    label = "label_example" # str | 
    username = "username_example" # str | Username to add

    # example passing only required values which don't have defaults set
    try:
        # Adds a user to a group
        api_response = api_instance.add_user_to_group(namespace, label, username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->add_user_to_group: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **label** | **str**|  |
 **username** | **str**| Username to add |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_model**
> Message create_model(namespace, model, data)

Registers a new model in the given namespace

Requires write permission.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    model = "model_example" # str | 
    data = open('/path/to/file', 'rb') # file_type | Zip file containing files of model
    inex_string = "inex_string_example" # str | JSON string that describes which files will be included/excluded from results archive (if provided, inex_file takes precendence) (optional)
    arguments = [
        "arguments_example",
    ] # [str] | Arguments that will be passed to GAMS call (optional)
    text_entries = [
        "text_entries_example",
    ] # [str] | Files to store as text entries (optional)
    stream_entries = [
        "stream_entries_example",
    ] # [str] | Files to stream during execution (optional)
    run = "run_example" # str | Main GMS file to run, please include file extension as well. Will use model + '.gms' if not provided. (optional)
    protect_model_files = True # bool | Whether to protect model files from being overwritten by data files. (optional)
    user_groups = [
        "user_groups_example",
    ] # [str] | Restrict access to specific user groups (optional)
    inex_file = open('/path/to/file', 'rb') # file_type | JSON file that describes which files will be included/excluded from results archive (optional)

    # example passing only required values which don't have defaults set
    try:
        # Registers a new model in the given namespace
        api_response = api_instance.create_model(namespace, model, data)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->create_model: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Registers a new model in the given namespace
        api_response = api_instance.create_model(namespace, model, data, inex_string=inex_string, arguments=arguments, text_entries=text_entries, stream_entries=stream_entries, run=run, protect_model_files=protect_model_files, user_groups=user_groups, inex_file=inex_file)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->create_model: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **model** | **str**|  |
 **data** | **file_type**| Zip file containing files of model |
 **inex_string** | **str**| JSON string that describes which files will be included/excluded from results archive (if provided, inex_file takes precendence) | [optional]
 **arguments** | **[str]**| Arguments that will be passed to GAMS call | [optional]
 **text_entries** | **[str]**| Files to store as text entries | [optional]
 **stream_entries** | **[str]**| Files to stream during execution | [optional]
 **run** | **str**| Main GMS file to run, please include file extension as well. Will use model + &#39;.gms&#39; if not provided. | [optional]
 **protect_model_files** | **bool**| Whether to protect model files from being overwritten by data files. | [optional]
 **user_groups** | **[str]**| Restrict access to specific user groups | [optional]
 **inex_file** | **file_type**| JSON file that describes which files will be included/excluded from results archive | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful |  -  |
**400** | Bad input |  -  |
**402** | Namespace Disk Quota Reached |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace is not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_model_deprecated**
> Message create_model_deprecated(namespace, model, data)

Registers a new model in the given namespace

Requires write permission.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    model = "model_example" # str | 
    data = open('/path/to/file', 'rb') # file_type | Zip file containing files of model
    inex_string = "inex_string_example" # str | JSON string that describes which files will be included/excluded from results archive (if provided, inex_file takes precendence) (optional)
    arguments = [
        "arguments_example",
    ] # [str] | Arguments that will be passed to GAMS call (optional)
    text_entries = [
        "text_entries_example",
    ] # [str] | Files to store as text entries (optional)
    stream_entries = [
        "stream_entries_example",
    ] # [str] | Files to stream during execution (optional)
    run = "run_example" # str | Main GMS file to run, please include file extension as well. Will use model + '.gms' if not provided. (optional)
    protect_model_files = True # bool | Whether to protect model files from being overwritten by data files. (optional)
    user_groups = [
        "user_groups_example",
    ] # [str] | Restrict access to specific user groups (optional)
    inex_file = open('/path/to/file', 'rb') # file_type | JSON file that describes which files will be included/excluded from results archive (optional)

    # example passing only required values which don't have defaults set
    try:
        # Registers a new model in the given namespace
        api_response = api_instance.create_model_deprecated(namespace, model, data)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->create_model_deprecated: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Registers a new model in the given namespace
        api_response = api_instance.create_model_deprecated(namespace, model, data, inex_string=inex_string, arguments=arguments, text_entries=text_entries, stream_entries=stream_entries, run=run, protect_model_files=protect_model_files, user_groups=user_groups, inex_file=inex_file)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->create_model_deprecated: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **model** | **str**|  |
 **data** | **file_type**| Zip file containing files of model |
 **inex_string** | **str**| JSON string that describes which files will be included/excluded from results archive (if provided, inex_file takes precendence) | [optional]
 **arguments** | **[str]**| Arguments that will be passed to GAMS call | [optional]
 **text_entries** | **[str]**| Files to store as text entries | [optional]
 **stream_entries** | **[str]**| Files to stream during execution | [optional]
 **run** | **str**| Main GMS file to run, please include file extension as well. Will use model + &#39;.gms&#39; if not provided. | [optional]
 **protect_model_files** | **bool**| Whether to protect model files from being overwritten by data files. | [optional]
 **user_groups** | **[str]**| Restrict access to specific user groups | [optional]
 **inex_file** | **file_type**| JSON file that describes which files will be included/excluded from results archive | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful |  -  |
**400** | Bad input |  -  |
**402** | Namespace Disk Quota Reached |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_namespace**
> Message create_namespace(namespace)

Creates a namespace

Requires admin role

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    disk_quota = 0 # int | disk quota in bytes (optional)

    # example passing only required values which don't have defaults set
    try:
        # Creates a namespace
        api_response = api_instance.create_namespace(namespace)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->create_namespace: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Creates a namespace
        api_response = api_instance.create_namespace(namespace, disk_quota=disk_quota)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->create_namespace: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **disk_quota** | **int**| disk quota in bytes | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful |  -  |
**400** | Input error |  -  |
**403** | Unauthorized access |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_user_group**
> Message create_user_group(namespace, label)

Creates a new user group

Requires admin role or inviter role with write permissions on the namespace.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    label = "HqXzyCBw3_uufVPIPF" # str | Group label

    # example passing only required values which don't have defaults set
    try:
        # Creates a new user group
        api_response = api_instance.create_user_group(namespace, label)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->create_user_group: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **label** | **str**| Group label |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_model**
> Message delete_model(namespace, model)

Deletes a model registered in the namespace

Requires write permission.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    model = "model_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Deletes a model registered in the namespace
        api_response = api_instance.delete_model(namespace, model)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->delete_model: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **model** | **str**|  |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace is not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_model_deprecated**
> Message delete_model_deprecated(namespace, model)

Deletes a model registered in the namespace

Requires write permission.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    model = "model_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Deletes a model registered in the namespace
        api_response = api_instance.delete_model_deprecated(namespace, model)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->delete_model_deprecated: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **model** | **str**|  |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_namespace**
> Message delete_namespace(namespace)

Deletes a namespace

Models, permissions and results assigned to this namespace are deleted. If jobs are running in this namespace, they are aborted. Requires admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Deletes a namespace
        api_response = api_instance.delete_namespace(namespace)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->delete_namespace: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**403** | Unauthorized access |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_namespace_quota**
> Message delete_namespace_quota(namespace)

Deletes namespace disk quota

Requires admin role

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Deletes namespace disk quota
        api_response = api_instance.delete_namespace_quota(namespace)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->delete_namespace_quota: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Input error |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_user_group**
> Message delete_user_group(namespace, label)

Deletes a user group

Inviters with write permission to the namespace can delete their own groups as well as the groups of any invitees. Admins can delete any group.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    label = "HqXzyCBw3_uufVPIPF" # str | Group label

    # example passing only required values which don't have defaults set
    try:
        # Deletes a user group
        api_response = api_instance.delete_user_group(namespace, label)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->delete_user_group: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **label** | **str**| Group label |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_accessible_namespaces**
> [NamespaceWithPermission] get_accessible_namespaces()

Lists the namespaces where the user has a non-zero permission, along with that `permission` and `disk_quota`

If the user does not have write permission, then the `disk_quota` is None. If the user has write permission but `disk_quota` is still None, it means namespace does not have any disk quota.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.namespace_with_permission import NamespaceWithPermission
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Lists the namespaces where the user has a non-zero permission, along with that `permission` and `disk_quota`
        api_response = api_instance.get_accessible_namespaces()
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->get_accessible_namespaces: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[NamespaceWithPermission]**](NamespaceWithPermission.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_model**
> file_type get_model(namespace, model)

Downloads model data

Requires read permission.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    model = "model_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Downloads model data
        api_response = api_instance.get_model(namespace, model)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->get_model: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **model** | **str**|  |

### Return type

**file_type**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  * content-disposition - attachment; filename&#x3D;model.zip <br>  * content-type - application/zip <br>  |
**403** | Unauthorized access |  -  |
**404** | Namespace is not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_model_deprecated**
> file_type get_model_deprecated(namespace, model)

Downloads model data

Requires read permission.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    model = "model_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Downloads model data
        api_response = api_instance.get_model_deprecated(namespace, model)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->get_model_deprecated: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **model** | **str**|  |

### Return type

**file_type**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  * content-disposition - attachment; filename&#x3D;model.zip <br>  * content-type - application/zip <br>  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_my_permissions**
> PermAndUsername get_my_permissions(namespace)

Returns the permissions of the logged in user in the given namespace

If user has admin role, it always returns 7.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from gams_engine.model.perm_and_username import PermAndUsername
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Returns the permissions of the logged in user in the given namespace
        api_response = api_instance.get_my_permissions(namespace)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->get_my_permissions: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |

### Return type

[**PermAndUsername**](PermAndUsername.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**404** | Namespace not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_namespace_quota**
> NamespaceQuota get_namespace_quota(namespace)

Displays namespace disk quota

Requires write permission

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from gams_engine.model.namespace_quota import NamespaceQuota
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Displays namespace disk quota
        api_response = api_instance.get_namespace_quota(namespace)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->get_namespace_quota: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |

### Return type

[**NamespaceQuota**](NamespaceQuota.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_groups**
> [UserGroups] get_user_groups(namespace)

Fetches user groups

If user, fetches groups in which the user is a member. If inviter, fetches groups the inviter is a member of, as well as all groups of invitees. If admin, fetches all groups.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from gams_engine.model.user_groups import UserGroups
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Fetches user groups
        api_response = api_instance.get_user_groups(namespace)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->get_user_groups: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |

### Return type

[**[UserGroups]**](UserGroups.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_permission**
> PermAndUsername get_user_permission(namespace, username)

Gets permissions of the given user for the given namespace

Admins can query all users, inviters can only query the invitees. Users can query themselves.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from gams_engine.model.perm_and_username import PermAndUsername
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    username = "username_example" # str | Username

    # example passing only required values which don't have defaults set
    try:
        # Gets permissions of the given user for the given namespace
        api_response = api_instance.get_user_permission(namespace, username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->get_user_permission: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **username** | **str**| Username |

### Return type

[**PermAndUsername**](PermAndUsername.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | User or namespace not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_models**
> [Models] list_models(namespace)

Lists the models in a namespace

If the model argument is specified, the information about this model is displayed only if the model is registered in the namespace. Models that are not assigned to a user group are visible to anyone with any permission (read, write, and/or execute) in the namespace. Models assigned to one or more user groups are only visible if the user can see any of these user groups. `protect_model_files` field is hidden by default for compatibility reasons, please use X-Fields header to get it. For example: X-Fields: \\*

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from gams_engine.model.models import Models
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    x_fields = "X-Fields_example" # str |  (optional)
    model = "model_example" # str | Name of the model to filter (optional)

    # example passing only required values which don't have defaults set
    try:
        # Lists the models in a namespace
        api_response = api_instance.list_models(namespace)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->list_models: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Lists the models in a namespace
        api_response = api_instance.list_models(namespace, x_fields=x_fields, model=model)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->list_models: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **x_fields** | **str**|  | [optional]
 **model** | **str**| Name of the model to filter | [optional]

### Return type

[**[Models]**](Models.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_namespaces**
> [Namespace] list_namespaces()

Lists the namespaces

You must have some permission on namespace to see it. Admins can see every namespace and every permission. Inviters can see namespaces for which they have permission, as well as the permissions of invitees. Users can see namespaces in which they have permissions, as well as the permissions for themselves.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.namespace import Namespace
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Lists the namespaces
        api_response = api_instance.list_namespaces()
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->list_namespaces: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[Namespace]**](Namespace.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**403** | Unauthorized access |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **remove_user_from_group**
> Message remove_user_from_group(namespace, label, username)

Removes a user from a group

Inviters can remove any invitees from a group they are a member of or that belongs to an invitee. Admins can remove anyone from any group.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    label = "label_example" # str | 
    username = "username_example" # str | Username to remove

    # example passing only required values which don't have defaults set
    try:
        # Removes a user from a group
        api_response = api_instance.remove_user_from_group(namespace, label, username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->remove_user_from_group: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **label** | **str**|  |
 **username** | **str**| Username to remove |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **replace_user_permission**
> Message replace_user_permission(namespace, username, permissions)

Sets permissions of the given user for the given namespace

Admins can grant all permissions, inviting persons can only grant the permissions they have. Requires admin or inviter role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    username = "username_example" # str | Username
    permissions = 0 # int | 4 - Read access to namespace 2 - Write access to namespace(delete, update, create) 1 - Execute model access to namespace  Sum to calculate permission

    # example passing only required values which don't have defaults set
    try:
        # Sets permissions of the given user for the given namespace
        api_response = api_instance.replace_user_permission(namespace, username, permissions)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->replace_user_permission: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **username** | **str**| Username |
 **permissions** | **int**| 4 - Read access to namespace 2 - Write access to namespace(delete, update, create) 1 - Execute model access to namespace  Sum to calculate permission |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_model**
> Message update_model(namespace, model)

Partially update registered models

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    model = "model_example" # str | 
    inex_string = "inex_string_example" # str | JSON string that describes which files will be included/excluded from results archive (if provided, inex_file takes precendence) (optional)
    arguments = [
        "arguments_example",
    ] # [str] | Arguments that will be passed to GAMS call (optional)
    run = "run_example" # str | Main GMS file to run, please include file extension as well. Will use model + '.gms' if not provided. (optional)
    protect_model_files = True # bool | Whether to protect model files from being overwritten by data files. (optional)
    user_groups = [
        "user_groups_example",
    ] # [str] | Restrict access to specific user groups (optional)
    text_entries = [
        "text_entries_example",
    ] # [str] | Files to store as text entries (optional)
    stream_entries = [
        "stream_entries_example",
    ] # [str] | Files to stream during execution (optional)
    delete_inex_file = False # bool | If `inex_file` is provided, setting this throws error, otherwise setting this to true deletes the inex file (optional) if omitted the server will use the default value of False
    delete_arguments = False # bool | If `arguments` is provided, setting this throws error, otherwise setting this deletes the arguments (optional) if omitted the server will use the default value of False
    delete_run = False # bool | If `run` is provided, setting this throws error, otherwise setting this to true deletes the run filename (optional) if omitted the server will use the default value of False
    delete_user_groups = False # bool | If `user_groups` is provided, setting this throws error, otherwise setting this deletes the user groups (optional) if omitted the server will use the default value of False
    delete_text_entries = False # bool | If `text_entries` is provided, setting this throws error, otherwise setting this deletes the text entries (optional) if omitted the server will use the default value of False
    delete_stream_entries = False # bool | If `stream_entries` is provided, setting this throws error, otherwise setting this deletes the stream entries (optional) if omitted the server will use the default value of False
    data = open('/path/to/file', 'rb') # file_type | Zip file containing files of model (optional)
    inex_file = open('/path/to/file', 'rb') # file_type | JSON file that describes which files will be included/excluded from results archive (optional)

    # example passing only required values which don't have defaults set
    try:
        # Partially update registered models
        api_response = api_instance.update_model(namespace, model)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->update_model: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Partially update registered models
        api_response = api_instance.update_model(namespace, model, inex_string=inex_string, arguments=arguments, run=run, protect_model_files=protect_model_files, user_groups=user_groups, text_entries=text_entries, stream_entries=stream_entries, delete_inex_file=delete_inex_file, delete_arguments=delete_arguments, delete_run=delete_run, delete_user_groups=delete_user_groups, delete_text_entries=delete_text_entries, delete_stream_entries=delete_stream_entries, data=data, inex_file=inex_file)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->update_model: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **model** | **str**|  |
 **inex_string** | **str**| JSON string that describes which files will be included/excluded from results archive (if provided, inex_file takes precendence) | [optional]
 **arguments** | **[str]**| Arguments that will be passed to GAMS call | [optional]
 **run** | **str**| Main GMS file to run, please include file extension as well. Will use model + &#39;.gms&#39; if not provided. | [optional]
 **protect_model_files** | **bool**| Whether to protect model files from being overwritten by data files. | [optional]
 **user_groups** | **[str]**| Restrict access to specific user groups | [optional]
 **text_entries** | **[str]**| Files to store as text entries | [optional]
 **stream_entries** | **[str]**| Files to stream during execution | [optional]
 **delete_inex_file** | **bool**| If &#x60;inex_file&#x60; is provided, setting this throws error, otherwise setting this to true deletes the inex file | [optional] if omitted the server will use the default value of False
 **delete_arguments** | **bool**| If &#x60;arguments&#x60; is provided, setting this throws error, otherwise setting this deletes the arguments | [optional] if omitted the server will use the default value of False
 **delete_run** | **bool**| If &#x60;run&#x60; is provided, setting this throws error, otherwise setting this to true deletes the run filename | [optional] if omitted the server will use the default value of False
 **delete_user_groups** | **bool**| If &#x60;user_groups&#x60; is provided, setting this throws error, otherwise setting this deletes the user groups | [optional] if omitted the server will use the default value of False
 **delete_text_entries** | **bool**| If &#x60;text_entries&#x60; is provided, setting this throws error, otherwise setting this deletes the text entries | [optional] if omitted the server will use the default value of False
 **delete_stream_entries** | **bool**| If &#x60;stream_entries&#x60; is provided, setting this throws error, otherwise setting this deletes the stream entries | [optional] if omitted the server will use the default value of False
 **data** | **file_type**| Zip file containing files of model | [optional]
 **inex_file** | **file_type**| JSON file that describes which files will be included/excluded from results archive | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Input error |  -  |
**402** | Namespace Disk Quota Reached |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace is not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_model_deprecated**
> Message update_model_deprecated(namespace, model)

Partially update registered models

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 
    model = "model_example" # str | 
    inex_string = "inex_string_example" # str | JSON string that describes which files will be included/excluded from results archive (if provided, inex_file takes precendence) (optional)
    arguments = [
        "arguments_example",
    ] # [str] | Arguments that will be passed to GAMS call (optional)
    run = "run_example" # str | Main GMS file to run, please include file extension as well. Will use model + '.gms' if not provided. (optional)
    protect_model_files = True # bool | Whether to protect model files from being overwritten by data files. (optional)
    user_groups = [
        "user_groups_example",
    ] # [str] | Restrict access to specific user groups (optional)
    text_entries = [
        "text_entries_example",
    ] # [str] | Files to store as text entries (optional)
    stream_entries = [
        "stream_entries_example",
    ] # [str] | Files to stream during execution (optional)
    delete_inex_file = False # bool | If `inex_file` is provided, setting this throws error, otherwise setting this to true deletes the inex file (optional) if omitted the server will use the default value of False
    delete_arguments = False # bool | If `arguments` is provided, setting this throws error, otherwise setting this deletes the arguments (optional) if omitted the server will use the default value of False
    delete_run = False # bool | If `run` is provided, setting this throws error, otherwise setting this to true deletes the run filename (optional) if omitted the server will use the default value of False
    delete_user_groups = False # bool | If `user_groups` is provided, setting this throws error, otherwise setting this deletes the user groups (optional) if omitted the server will use the default value of False
    delete_text_entries = False # bool | If `text_entries` is provided, setting this throws error, otherwise setting this deletes the text entries (optional) if omitted the server will use the default value of False
    delete_stream_entries = False # bool | If `stream_entries` is provided, setting this throws error, otherwise setting this deletes the stream entries (optional) if omitted the server will use the default value of False
    data = open('/path/to/file', 'rb') # file_type | Zip file containing files of model (optional)
    inex_file = open('/path/to/file', 'rb') # file_type | JSON file that describes which files will be included/excluded from results archive (optional)

    # example passing only required values which don't have defaults set
    try:
        # Partially update registered models
        api_response = api_instance.update_model_deprecated(namespace, model)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->update_model_deprecated: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Partially update registered models
        api_response = api_instance.update_model_deprecated(namespace, model, inex_string=inex_string, arguments=arguments, run=run, protect_model_files=protect_model_files, user_groups=user_groups, text_entries=text_entries, stream_entries=stream_entries, delete_inex_file=delete_inex_file, delete_arguments=delete_arguments, delete_run=delete_run, delete_user_groups=delete_user_groups, delete_text_entries=delete_text_entries, delete_stream_entries=delete_stream_entries, data=data, inex_file=inex_file)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->update_model_deprecated: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **model** | **str**|  |
 **inex_string** | **str**| JSON string that describes which files will be included/excluded from results archive (if provided, inex_file takes precendence) | [optional]
 **arguments** | **[str]**| Arguments that will be passed to GAMS call | [optional]
 **run** | **str**| Main GMS file to run, please include file extension as well. Will use model + &#39;.gms&#39; if not provided. | [optional]
 **protect_model_files** | **bool**| Whether to protect model files from being overwritten by data files. | [optional]
 **user_groups** | **[str]**| Restrict access to specific user groups | [optional]
 **text_entries** | **[str]**| Files to store as text entries | [optional]
 **stream_entries** | **[str]**| Files to stream during execution | [optional]
 **delete_inex_file** | **bool**| If &#x60;inex_file&#x60; is provided, setting this throws error, otherwise setting this to true deletes the inex file | [optional] if omitted the server will use the default value of False
 **delete_arguments** | **bool**| If &#x60;arguments&#x60; is provided, setting this throws error, otherwise setting this deletes the arguments | [optional] if omitted the server will use the default value of False
 **delete_run** | **bool**| If &#x60;run&#x60; is provided, setting this throws error, otherwise setting this to true deletes the run filename | [optional] if omitted the server will use the default value of False
 **delete_user_groups** | **bool**| If &#x60;user_groups&#x60; is provided, setting this throws error, otherwise setting this deletes the user groups | [optional] if omitted the server will use the default value of False
 **delete_text_entries** | **bool**| If &#x60;text_entries&#x60; is provided, setting this throws error, otherwise setting this deletes the text entries | [optional] if omitted the server will use the default value of False
 **delete_stream_entries** | **bool**| If &#x60;stream_entries&#x60; is provided, setting this throws error, otherwise setting this deletes the stream entries | [optional] if omitted the server will use the default value of False
 **data** | **file_type**| Zip file containing files of model | [optional]
 **inex_file** | **file_type**| JSON file that describes which files will be included/excluded from results archive | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Input error |  -  |
**402** | Namespace Disk Quota Reached |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_namespace_quota**
> Message update_namespace_quota(namespace, )

Updates namespace disk quota

Requires admin role

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import namespaces_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = namespaces_api.NamespacesApi(api_client)
    namespace = "namespace_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Updates namespace disk quota
        api_response = api_instance.update_namespace_quota(namespace, )
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling NamespacesApi->update_namespace_quota: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **namespace** | **str**|  |
 **disk_quota** | **int**| disk quota in bytes | defaults to 1000000000

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Input error |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

