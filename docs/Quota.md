# Quota


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disk_quota** | **int, none_type** |  | [optional] 
**disk_used** | **int, none_type** |  | [optional] 
**parallel_quota** | **float, none_type** |  | [optional] 
**username** | **str** |  | [optional] 
**volume_quota** | **float, none_type** |  | [optional] 
**volume_used** | **float** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


