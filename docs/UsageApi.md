# gams_engine.UsageApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_instance**](UsageApi.md#create_instance) | **POST** /usage/instances | Adds new instance
[**delete_instance**](UsageApi.md#delete_instance) | **DELETE** /usage/instances | Deletes existing instance
[**delete_quota**](UsageApi.md#delete_quota) | **DELETE** /usage/quota | Deletes the quota that is bound to the given user
[**delete_user_instances**](UsageApi.md#delete_user_instances) | **DELETE** /usage/instances/{username} | Deletes instances assigned to a user
[**get_instances**](UsageApi.md#get_instances) | **GET** /usage/instances | Lists all instances
[**get_quota**](UsageApi.md#get_quota) | **GET** /usage/quota | Gets quotas for the given user
[**get_usage**](UsageApi.md#get_usage) | **GET** /usage/ | Queries the usage data of a user with or without the user&#39;s invitees
[**get_user_instances**](UsageApi.md#get_user_instances) | **GET** /usage/instances/{username} | Lists all instances user has permissions to use as well as the default instance
[**update_instance**](UsageApi.md#update_instance) | **PUT** /usage/instances | Updates existing instance
[**update_quota**](UsageApi.md#update_quota) | **PUT** /usage/quota | Updates the quota that is bound to the given user
[**update_user_default_instance**](UsageApi.md#update_user_default_instance) | **PUT** /usage/instances/{username}/default | Updates the default instance of the logged in user
[**update_user_instances**](UsageApi.md#update_user_instances) | **PUT** /usage/instances/{username} | Updates instances a user has permissions to use


# **create_instance**
> Message create_instance(label, cpu_request, memory_request, multiplier, workspace_request)

Adds new instance

Requires admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    label = "label_example" # str | Label to assign to instance
    cpu_request = 0 # float | CPU units (vCPU/Core, Hyperthread)
    memory_request = 0 # int | Memory units (MiB)
    multiplier = 0 # float | Instance multiplier
    workspace_request = 100 # int | Max amount of disk space that can be used during the solve (MiB)
    tolerations = [
        "tolerations_example",
    ] # [str] | Toleration information for the pod (optional)
    node_selectors = [
        "node_selectors_example",
    ] # [str] | Node selector information for the pod (optional)

    # example passing only required values which don't have defaults set
    try:
        # Adds new instance
        api_response = api_instance.create_instance(label, cpu_request, memory_request, multiplier, workspace_request)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->create_instance: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Adds new instance
        api_response = api_instance.create_instance(label, cpu_request, memory_request, multiplier, workspace_request, tolerations=tolerations, node_selectors=node_selectors)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->create_instance: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **label** | **str**| Label to assign to instance |
 **cpu_request** | **float**| CPU units (vCPU/Core, Hyperthread) |
 **memory_request** | **int**| Memory units (MiB) |
 **multiplier** | **float**| Instance multiplier |
 **workspace_request** | **int**| Max amount of disk space that can be used during the solve (MiB) |
 **tolerations** | **[str]**| Toleration information for the pod | [optional]
 **node_selectors** | **[str]**| Node selector information for the pod | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_instance**
> Message delete_instance(label)

Deletes existing instance

Requires admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    label = "label_example" # str | Label of instance to delete

    # example passing only required values which don't have defaults set
    try:
        # Deletes existing instance
        api_response = api_instance.delete_instance(label)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->delete_instance: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **label** | **str**| Label of instance to delete |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Instance not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_quota**
> Message delete_quota(username)

Deletes the quota that is bound to the given user

Admins can delete quotas of everyone. Inviters can delete quotas of invitees.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    username = "username_example" # str | Name of the user
    field = "volume_quota" # str | Name of the field to delete, if not provided all fields are deleted (optional)

    # example passing only required values which don't have defaults set
    try:
        # Deletes the quota that is bound to the given user
        api_response = api_instance.delete_quota(username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->delete_quota: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Deletes the quota that is bound to the given user
        api_response = api_instance.delete_quota(username, field=field)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->delete_quota: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| Name of the user |
 **field** | **str**| Name of the field to delete, if not provided all fields are deleted | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_user_instances**
> Message delete_user_instances(username)

Deletes instances assigned to a user

Admins can delete instances of anyone. Inviters can delete instances of direct/indirect invitees.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    username = "username_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Deletes instances assigned to a user
        api_response = api_instance.delete_user_instances(username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->delete_user_instances: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**|  |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_instances**
> [ModelInstanceInfo] get_instances()

Lists all instances

Requires admin role or the user must be able to use raw resource requests.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from gams_engine.model.model_instance_info import ModelInstanceInfo
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Lists all instances
        api_response = api_instance.get_instances()
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->get_instances: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[ModelInstanceInfo]**](ModelInstanceInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_quota**
> [Quota] get_quota(username)

Gets quotas for the given user

Users inherit quotas from their (grand) inviters as well. Admins can query the quotas of everyone. Inviters can query the quotas of themselves and invitees. Users can query quotas for themselves.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from gams_engine.model.quota import Quota
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    username = "username_example" # str | Name of the user

    # example passing only required values which don't have defaults set
    try:
        # Gets quotas for the given user
        api_response = api_instance.get_quota(username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->get_quota: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| Name of the user |

### Return type

[**[Quota]**](Quota.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_usage**
> ModelUsage get_usage(username)

Queries the usage data of a user with or without the user's invitees

Admins can query everyone's usage data. Inviters can query usage data of invitees or usage data of jobs they have access to. Users can only query their own usage data or usage data of jobs they have access to. `resource_warning`, `instance`, and `multiplier` fields are hidden by default for compatibility reasons, please use X-Fields header to get it. For example: X-Fields: job_usage{\\*, labels{\\*}}, hypercube_job_usage{\\*, labels{\\*}}

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from gams_engine.model.model_usage import ModelUsage
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    username = "username_example" # str | Name of the user
    recursive = False # bool | Show invitees as well (optional) if omitted the server will use the default value of False
    from_datetime = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | iso8601 Datetime to filter jobs out when they are submitted before. (optional)
    to_datetime = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | iso8601 Datetime to filter jobs out when they are submitted after. (optional)
    x_fields = "X-Fields_example" # str |  (optional)
    token = [
        "token_example",
    ] # [str] | Job tokens to filter (optional)
    hypercube_token = [
        "hypercube_token_example",
    ] # [str] | Hypercube tokens to filter (optional)

    # example passing only required values which don't have defaults set
    try:
        # Queries the usage data of a user with or without the user's invitees
        api_response = api_instance.get_usage(username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->get_usage: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Queries the usage data of a user with or without the user's invitees
        api_response = api_instance.get_usage(username, recursive=recursive, from_datetime=from_datetime, to_datetime=to_datetime, x_fields=x_fields, token=token, hypercube_token=hypercube_token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->get_usage: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| Name of the user |
 **recursive** | **bool**| Show invitees as well | [optional] if omitted the server will use the default value of False
 **from_datetime** | **datetime**| iso8601 Datetime to filter jobs out when they are submitted before. | [optional]
 **to_datetime** | **datetime**| iso8601 Datetime to filter jobs out when they are submitted after. | [optional]
 **x_fields** | **str**|  | [optional]
 **token** | **[str]**| Job tokens to filter | [optional]
 **hypercube_token** | **[str]**| Hypercube tokens to filter | [optional]

### Return type

[**ModelUsage**](ModelUsage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_instances**
> ModelUserinstanceInfo get_user_instances(username)

Lists all instances user has permissions to use as well as the default instance

Admins list instances of anyone. Inviters can list instances of direct/indirect invitees. Users can list own instances

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from gams_engine.model.model_userinstance_info import ModelUserinstanceInfo
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    username = "username_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Lists all instances user has permissions to use as well as the default instance
        api_response = api_instance.get_user_instances(username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->get_user_instances: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**|  |

### Return type

[**ModelUserinstanceInfo**](ModelUserinstanceInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_instance**
> Message update_instance(label, cpu_request, memory_request, multiplier, workspace_request, old_label)

Updates existing instance

Requires admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    label = "label_example" # str | Label to assign to instance
    cpu_request = 0 # float | CPU units (vCPU/Core, Hyperthread)
    memory_request = 0 # int | Memory units (MiB)
    multiplier = 0 # float | Instance multiplier
    workspace_request = 100 # int | Max amount of disk space that can be used during the solve (MiB)
    old_label = "old_label_example" # str | Label of instance to modify
    tolerations = [
        "tolerations_example",
    ] # [str] | Toleration information for the pod (optional)
    node_selectors = [
        "node_selectors_example",
    ] # [str] | Node selector information for the pod (optional)

    # example passing only required values which don't have defaults set
    try:
        # Updates existing instance
        api_response = api_instance.update_instance(label, cpu_request, memory_request, multiplier, workspace_request, old_label)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->update_instance: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Updates existing instance
        api_response = api_instance.update_instance(label, cpu_request, memory_request, multiplier, workspace_request, old_label, tolerations=tolerations, node_selectors=node_selectors)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->update_instance: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **label** | **str**| Label to assign to instance |
 **cpu_request** | **float**| CPU units (vCPU/Core, Hyperthread) |
 **memory_request** | **int**| Memory units (MiB) |
 **multiplier** | **float**| Instance multiplier |
 **workspace_request** | **int**| Max amount of disk space that can be used during the solve (MiB) |
 **old_label** | **str**| Label of instance to modify |
 **tolerations** | **[str]**| Toleration information for the pod | [optional]
 **node_selectors** | **[str]**| Node selector information for the pod | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Instance not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_quota**
> Message update_quota(username)

Updates the quota that is bound to the given user

If the user is inviter, everyone in the user's subtree is also effected. Admins cannot have quotas. Admins can update quotas of everyone. Inviters can update quotas of invitees.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    username = "username_example" # str | Name of the user
    parallel_quota = 0 # float | Number of GAMS Seconds that the user and the user's invitees can run in parallel (optional)
    volume_quota = 0 # float | Number of GAMS Seconds that the user and the user's invitees can run in total (optional)
    disk_quota = 0 # int | Storage in bytes that the user and the user's invitees can use in total (optional)

    # example passing only required values which don't have defaults set
    try:
        # Updates the quota that is bound to the given user
        api_response = api_instance.update_quota(username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->update_quota: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Updates the quota that is bound to the given user
        api_response = api_instance.update_quota(username, parallel_quota=parallel_quota, volume_quota=volume_quota, disk_quota=disk_quota)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->update_quota: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| Name of the user |
 **parallel_quota** | **float**| Number of GAMS Seconds that the user and the user&#39;s invitees can run in parallel | [optional]
 **volume_quota** | **float**| Number of GAMS Seconds that the user and the user&#39;s invitees can run in total | [optional]
 **disk_quota** | **int**| Storage in bytes that the user and the user&#39;s invitees can use in total | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_user_default_instance**
> Message update_user_default_instance(username, default_label)

Updates the default instance of the logged in user

`username` must match the logged in user and instance specified must be available to the user.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    username = "username_example" # str | 
    default_label = "default_label_example" # str | Label of the instance to be used as default

    # example passing only required values which don't have defaults set
    try:
        # Updates the default instance of the logged in user
        api_response = api_instance.update_user_default_instance(username, default_label)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->update_user_default_instance: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**|  |
 **default_label** | **str**| Label of the instance to be used as default |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Default instance not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_user_instances**
> Message update_user_instances(username, default_label)

Updates instances a user has permissions to use

Admins can update instances of anyone. Admin can assign any existing instances. Admins cannot be assigned instances (only default instance). Inviters can update instances of direct/indirect invitees. Inviters can only assign instances that have been assigned to them.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import usage_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = usage_api.UsageApi(api_client)
    username = "username_example" # str | 
    default_label = "default_label_example" # str | Label of the instance to be used as default
    labels = [
        "labels_example",
    ] # [str] | Instances to assign to user (optional)

    # example passing only required values which don't have defaults set
    try:
        # Updates instances a user has permissions to use
        api_response = api_instance.update_user_instances(username, default_label)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->update_user_instances: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Updates instances a user has permissions to use
        api_response = api_instance.update_user_instances(username, default_label, labels=labels)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsageApi->update_user_instances: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**|  |
 **default_label** | **str**| Label of the instance to be used as default |
 **labels** | **[str]**| Instances to assign to user | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

