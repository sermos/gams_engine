# User


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deleted** | **bool** |  | [optional] 
**invitation_time** | **datetime, none_type** |  | [optional] 
**inviter_name** | **str, none_type** |  | [optional] 
**old_username** | **str, none_type** |  | [optional] 
**roles** | **[str]** |  | [optional] 
**username** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


