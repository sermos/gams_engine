# gams_engine.UsersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_invitation**](UsersApi.md#create_invitation) | **POST** /users/invitation | Creates an invitation code for new user
[**create_webhook**](UsersApi.md#create_webhook) | **POST** /users/webhooks | Creates a new webhook
[**delete_invitation**](UsersApi.md#delete_invitation) | **DELETE** /users/invitation | Deletes an invitation if it is unused
[**delete_user**](UsersApi.md#delete_user) | **DELETE** /users/ | Deletes a user
[**delete_webhook**](UsersApi.md#delete_webhook) | **DELETE** /users/webhooks | Removes an existing webhook
[**list_invitations**](UsersApi.md#list_invitations) | **GET** /users/invitation | Lists the invitation codes created by the user
[**list_users**](UsersApi.md#list_users) | **GET** /users/ | If admin, lists all users
[**list_webhooks**](UsersApi.md#list_webhooks) | **GET** /users/webhooks | If admin, lists all webhooks
[**register**](UsersApi.md#register) | **POST** /users/ | Creates a new user if the invitation code is valid and not used
[**replace_user_name**](UsersApi.md#replace_user_name) | **PUT** /users/username | Changes username of the user with the given username
[**replace_user_password**](UsersApi.md#replace_user_password) | **PUT** /users/ | Changes password of the user with the given username
[**update_user_role**](UsersApi.md#update_user_role) | **PUT** /users/role | Promotes/demotes user


# **create_invitation**
> InvitationToken create_invitation()

Creates an invitation code for new user

Admins can invite admins, inviters and normal users. Inviters can invite inviters and normal users. Inviters cannot grant permissions that they do not posses. Only admins can specify `gams_license`. Requires admin or inviter role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.message import Message
from gams_engine.model.invitation_token import InvitationToken
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    roles = [
        "inviter",
    ] # [str] | Possible values are: ['inviter', 'admin'] (optional)
    namespace_permissions = [
        "namespace_permissions_example",
    ] # [str] | Syntax is permission_number@namespace_name, ie \\\"7@global\\\" (optional)
    parallel_quota = 0 # float | Number of GAMS Seconds that the user and the user's invitees can run in parallel (optional)
    volume_quota = 0 # float | Number of GAMS Seconds that the user and the user's invitees can run in total (optional)
    disk_quota = 0 # int | Storage space in bytes that the user and the user's invitees can use in total (optional)
    labels = [
        "labels_example",
    ] # [str] | Instances to assign to user (optional)
    default_label = "default_label_example" # str | Label of the instance to be used as default (optional)
    user_groups = [
        "user_groups_example",
    ] # [str] | User groups to put the user in, in format user_group@namespace format (optional)
    gams_license = "gams_license_example" # str | Base64 encoded GAMS license to attach to user (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Creates an invitation code for new user
        api_response = api_instance.create_invitation(roles=roles, namespace_permissions=namespace_permissions, parallel_quota=parallel_quota, volume_quota=volume_quota, disk_quota=disk_quota, labels=labels, default_label=default_label, user_groups=user_groups, gams_license=gams_license)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->create_invitation: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roles** | **[str]**| Possible values are: [&#39;inviter&#39;, &#39;admin&#39;] | [optional]
 **namespace_permissions** | **[str]**| Syntax is permission_number@namespace_name, ie \\\&quot;7@global\\\&quot; | [optional]
 **parallel_quota** | **float**| Number of GAMS Seconds that the user and the user&#39;s invitees can run in parallel | [optional]
 **volume_quota** | **float**| Number of GAMS Seconds that the user and the user&#39;s invitees can run in total | [optional]
 **disk_quota** | **int**| Storage space in bytes that the user and the user&#39;s invitees can use in total | [optional]
 **labels** | **[str]**| Instances to assign to user | [optional]
 **default_label** | **str**| Label of the instance to be used as default | [optional]
 **user_groups** | **[str]**| User groups to put the user in, in format user_group@namespace format | [optional]
 **gams_license** | **str**| Base64 encoded GAMS license to attach to user | [optional]

### Return type

[**InvitationToken**](InvitationToken.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | Namespace not found |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_webhook**
> Message create_webhook(url)

Creates a new webhook

The webhook feature has to be enabled via the `/configuration` endpoint. In case the webhook_access configuration is set to `ADMIN_ONLY`, only admins are able to access this endpoint.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    url = "url_example" # str | The URL to which the payload is to be sent
    content_type = "form" # str | The media type used to serialize the data. Possible values are form and json (form is the default). (optional) if omitted the server will use the default value of "form"
    secret = "secret_example" # str | The secret key used to generate the HMAC. This HMAC will be included in the X-ENGINE-HMAC header. (optional)
    recursive = False # bool | Whether the webhook should also be registered for invitees. If the logged-in user is admin, recursive means that the webhook is registered globally for all users. (optional) if omitted the server will use the default value of False
    events = [
        "["ALL"]",
    ] # [str] | Events to subscribe to (default: all events). Possible values: ALL JOB_FINISHED HC_JOB_FINISHED (optional)
    insecure_ssl = False # bool | Whether to disable validation of SSL certificates when submitting this webhook. We strongly recommend not disabling certificate validation to protect against Man in The Middle attacks. (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    try:
        # Creates a new webhook
        api_response = api_instance.create_webhook(url)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->create_webhook: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Creates a new webhook
        api_response = api_instance.create_webhook(url, content_type=content_type, secret=secret, recursive=recursive, events=events, insecure_ssl=insecure_ssl)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->create_webhook: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **url** | **str**| The URL to which the payload is to be sent |
 **content_type** | **str**| The media type used to serialize the data. Possible values are form and json (form is the default). | [optional] if omitted the server will use the default value of "form"
 **secret** | **str**| The secret key used to generate the HMAC. This HMAC will be included in the X-ENGINE-HMAC header. | [optional]
 **recursive** | **bool**| Whether the webhook should also be registered for invitees. If the logged-in user is admin, recursive means that the webhook is registered globally for all users. | [optional] if omitted the server will use the default value of False
 **events** | **[str]**| Events to subscribe to (default: all events). Possible values: ALL JOB_FINISHED HC_JOB_FINISHED | [optional]
 **insecure_ssl** | **bool**| Whether to disable validation of SSL certificates when submitting this webhook. We strongly recommend not disabling certificate validation to protect against Man in The Middle attacks. | [optional] if omitted the server will use the default value of False

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_invitation**
> Message delete_invitation(token)

Deletes an invitation if it is unused

Admins can delete all unused invitations, inviters can only delete invitations that belong to them or that belong to direct/indirect invitees of them.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    token = "token_example" # str | Token of the invitation to be deleted

    # example passing only required values which don't have defaults set
    try:
        # Deletes an invitation if it is unused
        api_response = api_instance.delete_invitation(token)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->delete_invitation: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**| Token of the invitation to be deleted |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_user**
> Message delete_user(username)

Deletes a user

Admins can delete anyone, inviters can delete users who are directly/indirectly invited. Requires admin or inviter role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    username = "username_example" # str | Username of the user to delete
    delete_results = True # bool | Also delete all submission results that are related to the user (optional)
    delete_children = False # bool | Delete users invited directly/indirectly by this user (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    try:
        # Deletes a user
        api_response = api_instance.delete_user(username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->delete_user: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Deletes a user
        api_response = api_instance.delete_user(username, delete_results=delete_results, delete_children=delete_children)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->delete_user: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| Username of the user to delete |
 **delete_results** | **bool**| Also delete all submission results that are related to the user | [optional]
 **delete_children** | **bool**| Delete users invited directly/indirectly by this user | [optional] if omitted the server will use the default value of False

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_webhook**
> Message delete_webhook(id)

Removes an existing webhook

The webhook feature has to be enabled via the `/configuration` endpoint. In case the webhook_access configuration is set to `ADMIN_ONLY`, only admins are able to access this endpoint.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    id = 0 # int | The ID of the webhook to be removed

    # example passing only required values which don't have defaults set
    try:
        # Removes an existing webhook
        api_response = api_instance.delete_webhook(id)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->delete_webhook: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The ID of the webhook to be removed |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**401** | Invalid authentication |  -  |
**403** | Unauthorized access |  -  |
**404** | Webhook not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_invitations**
> [Invitation] list_invitations()

Lists the invitation codes created by the user

\"Everyone\" argument can only be set by the admin, and if it is set, all invitations are listed. Requires inviter or admin role.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.invitation import Invitation
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    token = "token_example" # str | Invitation code to filter (optional)
    everyone = True # bool | Can only be set by admin, lists all invitations (optional)
    x_fields = "X-Fields_example" # str |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Lists the invitation codes created by the user
        api_response = api_instance.list_invitations(token=token, everyone=everyone, x_fields=x_fields)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->list_invitations: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **str**| Invitation code to filter | [optional]
 **everyone** | **bool**| Can only be set by admin, lists all invitations | [optional]
 **x_fields** | **str**|  | [optional]

### Return type

[**[Invitation]**](Invitation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_users**
> [User] list_users()

If admin, lists all users

If inviter, lists users invited directly/indirectly. If normal user, shows details of logged-in user. Setting username filters the results.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.message import Message
from gams_engine.model.user import User
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    username = "username_example" # str | Username of the user to filter (optional)
    x_fields = "X-Fields_example" # str |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # If admin, lists all users
        api_response = api_instance.list_users(username=username, x_fields=x_fields)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->list_users: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| Username of the user to filter | [optional]
 **x_fields** | **str**|  | [optional]

### Return type

[**[User]**](User.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_webhooks**
> [Webhook] list_webhooks()

If admin, lists all webhooks

If inviter, lists webhooks of direct/indirect invitees. If user, lists webhooks of logged-in user.  The webhook feature has to be enabled via the `/configuration` endpoint. In case the webhook_access configuration is set to `ADMIN_ONLY`, only admins are able to access this endpoint.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.webhook import Webhook
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # If admin, lists all webhooks
        api_response = api_instance.list_webhooks()
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->list_webhooks: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[Webhook]**](Webhook.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Succesful |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **register**
> Message register(username, password, invitation_code)

Creates a new user if the invitation code is valid and not used

Username must be unique.

### Example


```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.bad_input import BadInput
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with gams_engine.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    username = "HqXzyCBw3_uufVPIPF" # str | 
    password = "k%?x!u'K}qz^sEC)lJ*=-jQ+'6`%cClu,k'!'su[.lzF6V,V6" # str | 
    invitation_code = "invitation_code_example" # str | Invitation code

    # example passing only required values which don't have defaults set
    try:
        # Creates a new user if the invitation code is valid and not used
        api_response = api_instance.register(username, password, invitation_code)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->register: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**|  |
 **password** | **str**|  |
 **invitation_code** | **str**| Invitation code |

### Return type

[**Message**](Message.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Success |  -  |
**400** | Bad Input |  -  |
**500** | Internal Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **replace_user_name**
> Message replace_user_name(username, new_username)

Changes username of the user with the given username

Requires admin permissions.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    username = "username_example" # str | Username of the user whose username is to be changed
    new_username = "HqXzyCBw3_uufVPIPF" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Changes username of the user with the given username
        api_response = api_instance.replace_user_name(username, new_username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->replace_user_name: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| Username of the user whose username is to be changed |
 **new_username** | **str**|  |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |
**429** | Username changed recently |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **replace_user_password**
> Message replace_user_password(username, password)

Changes password of the user with the given username

Users can change their own passwords, admins can change the password of anyone.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.bad_input import BadInput
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    username = "username_example" # str | Username of the user whose password is to be changed
    password = "k%?x!u'K}qz^sEC)lJ*=-jQ+'6`%cClu,k'!'su[.lzF6V,V6" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Changes password of the user with the given username
        api_response = api_instance.replace_user_password(username, password)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->replace_user_password: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| Username of the user whose password is to be changed |
 **password** | **str**|  |

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_user_role**
> Message update_user_role(username)

Promotes/demotes user

Admins can modify anyone. Inviters can modify direct/indirect invitees.

### Example

* Basic Authentication (BasicAuth):

```python
import time
import gams_engine
from gams_engine.api import users_api
from gams_engine.model.message import Message
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = gams_engine.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = gams_engine.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with gams_engine.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    username = "username_example" # str | Username of the user whose roles are to be changed
    roles = [
        "inviter",
    ] # [str] | New roles of the user, possible values are: ['inviter', 'admin'] (optional)

    # example passing only required values which don't have defaults set
    try:
        # Promotes/demotes user
        api_response = api_instance.update_user_role(username)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->update_user_role: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Promotes/demotes user
        api_response = api_instance.update_user_role(username, roles=roles)
        pprint(api_response)
    except gams_engine.ApiException as e:
        print("Exception when calling UsersApi->update_user_role: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **str**| Username of the user whose roles are to be changed |
 **roles** | **[str]**| New roles of the user, possible values are: [&#39;inviter&#39;, &#39;admin&#39;] | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded, multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful |  -  |
**400** | Bad Input |  -  |
**403** | Unauthorized access |  -  |
**404** | User not found |  -  |
**500** | Internal error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

