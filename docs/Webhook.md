# Webhook


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content_type** | **str** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**events** | **[str]** |  | [optional] 
**id** | **int** |  | [optional] 
**insecure_ssl** | **bool** |  | [optional] 
**recursive** | **bool** |  | [optional] 
**url** | **str** |  | [optional] 
**username** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


