
# flake8: noqa

# Import all APIs into this package.
# If you have many APIs here with many many models used in each API this may
# raise a `RecursionError`.
# In order to avoid this, import only the API that you directly need like:
#
#   from .api.auth_api import AuthApi
#
# or import this package, but before doing it, use:
#
#   import sys
#   sys.setrecursionlimit(n)

# Import APIs into API package:
from gams_engine.api.auth_api import AuthApi
from gams_engine.api.cleanup_api import CleanupApi
from gams_engine.api.default_api import DefaultApi
from gams_engine.api.hypercube_api import HypercubeApi
from gams_engine.api.jobs_api import JobsApi
from gams_engine.api.licenses_api import LicensesApi
from gams_engine.api.namespaces_api import NamespacesApi
from gams_engine.api.usage_api import UsageApi
from gams_engine.api.users_api import UsersApi
