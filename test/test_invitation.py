"""
    GAMS Engine

    With GAMS Engine you can register and solve GAMS models. It has a namespace management system, so you can restrict your users to certain models.  # noqa: E501

    The version of the OpenAPI document: 22.06.03
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import gams_engine
from gams_engine.model.invitation_quota import InvitationQuota
globals()['InvitationQuota'] = InvitationQuota
from gams_engine.model.invitation import Invitation


class TestInvitation(unittest.TestCase):
    """Invitation unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testInvitation(self):
        """Test Invitation"""
        # FIXME: construct object with mandatory attributes with example values
        # model = Invitation()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
