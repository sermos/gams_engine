"""
    GAMS Engine

    With GAMS Engine you can register and solve GAMS models. It has a namespace management system, so you can restrict your users to certain models.  # noqa: E501

    The version of the OpenAPI document: 22.06.03
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import gams_engine
from gams_engine.model.license import License


class TestLicense(unittest.TestCase):
    """License unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testLicense(self):
        """Test License"""
        # FIXME: construct object with mandatory attributes with example values
        # model = License()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
