"""
    GAMS Engine

    With GAMS Engine you can register and solve GAMS models. It has a namespace management system, so you can restrict your users to certain models.  # noqa: E501

    The version of the OpenAPI document: 22.06.03
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import gams_engine
from gams_engine.model.namespace_with_permission import NamespaceWithPermission


class TestNamespaceWithPermission(unittest.TestCase):
    """NamespaceWithPermission unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testNamespaceWithPermission(self):
        """Test NamespaceWithPermission"""
        # FIXME: construct object with mandatory attributes with example values
        # model = NamespaceWithPermission()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
