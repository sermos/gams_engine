"""
    GAMS Engine

    With GAMS Engine you can register and solve GAMS models. It has a namespace management system, so you can restrict your users to certain models.  # noqa: E501

    The version of the OpenAPI document: 22.06.03
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import gams_engine
from gams_engine.model.user import User
from gams_engine.model.user_group_member import UserGroupMember
globals()['User'] = User
globals()['UserGroupMember'] = UserGroupMember
from gams_engine.model.user_groups import UserGroups


class TestUserGroups(unittest.TestCase):
    """UserGroups unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testUserGroups(self):
        """Test UserGroups"""
        # FIXME: construct object with mandatory attributes with example values
        # model = UserGroups()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
